﻿namespace _415_0603_Production_Tool_PI_PERL
{
    internal class _Variables
    {
        internal static string DongleSN;
        
        /// <summary>
        /// The serial number of the Pouch Inspector
        /// </summary>
        internal static string PISN;
        internal static string LockCode;
        internal static string LicType;
        internal static string BpCode;

        internal static string DvcCode;
        internal static string MoType;
    }
}