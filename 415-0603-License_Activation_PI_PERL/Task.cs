﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;

namespace _415_0603_Production_Tool_PI_PERL
{
    public abstract class Task
    {
        protected int currentStep = 0;
        public static Task CurrentTask;
        static Task[] taskList = new Task[(int)AllTasks.COUNT];
        public enum TaskStates
        {
            PENDING,
            PASS,
            FAIL,
            COUNT
        }
        protected int taskIndex;

        //Specified order.
        internal enum AllTasks
        {
            DONGLE_REQUEST,
            DONGLE_RECEIVE_DVC,
            DONGLE_RECEIVE_BP,
            PC_PROMPT_INPUT,
            PC_RENAME,
            PC_SPEED,
            DONGLE_PC_PAIR,
            PC_PLC_UPDATE,
            PC_THINGWORX,
            PC_FF_PREP,
            PI_SOFTWARE_INSTALLATION,
            PC_WINDOWS_ACTIVATE,
            CLEANUP,
            DRIVE_CHECK,
            PLC_CHECK,
            COUNT
        }

        internal static void INIT()
        {
            taskList[(int) AllTasks.DONGLE_REQUEST] = new Tasks.Task1_Request_Dongle_Activation();
            taskList[(int) AllTasks.DONGLE_RECEIVE_DVC] = new Tasks.Task2_Store_MVC_Dongle_Info();
            taskList[(int) AllTasks.DONGLE_RECEIVE_BP] = new Tasks.Task_Dongle_Store_BP_Info();
            taskList[(int) AllTasks.PC_RENAME] = new Tasks.Task4_PI_PC_Rename();
            taskList[(int) AllTasks.DONGLE_PC_PAIR] = new Tasks.Task5_Dongle_Pair_With_PI();
            taskList[(int) AllTasks.PC_PLC_UPDATE] = new Tasks.Task6_PI_PLC_Update();
            taskList[(int) AllTasks.PC_THINGWORX] = new Tasks.Task7_PI_ThingWorx_Install();
            taskList[(int) AllTasks.PC_WINDOWS_ACTIVATE] = new Tasks.Task_Windows_Activation();
            taskList[(int) AllTasks.PI_SOFTWARE_INSTALLATION] = new Tasks.Task_PI_Software_Setup();
            taskList[(int) AllTasks.CLEANUP] = new Tasks.Task_Cleanup();
            taskList[(int) AllTasks.PC_FF_PREP] = new Tasks.Task_FinalFunctionalPrep();
            taskList[(int) AllTasks.PC_SPEED] = new Tasks.Task_Configure_PI_Speed();
            taskList[(int)AllTasks.DRIVE_CHECK] = new Tasks.Task_Drive_Check();
            taskList[(int)AllTasks.PC_PROMPT_INPUT] = new Tasks.Task_PromptForUserInput();

            taskList[(int)AllTasks.PLC_CHECK] = new Tasks.Task_PLC_Differentiate();
        }


        internal static void DoTask(string tName, int resumeCode)
        {
            MainForm.ResetStepdisplay();

            Task toRun;
            for (int i = 0; i < taskList.Count(); i++)
            {
                toRun = taskList[i];
                if (toRun.TaskName == tName)
                {
                    toRun.Do(resumeCode);
                    break;
                }
            }

        }

        internal static string GetTaskName(int index)
        {
            return taskList[index].TaskName;
        }

        internal static int Count
        {
            get { return taskList.Count(); }
        }

        internal static bool IsTaskPiEnabled(int i)
        {
            return taskList[i].IsPiEnabled;
        }

        internal static bool IsOneOffEnabledAtIndex(int i)
        {
            return taskList[i].IsOneOffEnabled;
        }

        internal Task()
        {
            Initialize();
        }

        internal static string GetTaskNameAtIndex(AllTasks taskEnum)
        {
            return taskList[(int)taskEnum].TaskName;
        }

        internal abstract string TaskName { get; }


        internal void AddStep(string stepToAdd)
        {
            MainForm.AddStep(stepToAdd);
        }

        protected virtual void NextStep()
        {
            currentStep++;
            Do(currentStep);
        }

        protected void FinalStep()
        {
            currentStep = -1;
            Do(currentStep);
        }

        internal virtual void Complete() {
            MainForm.CompleteTask();
            if (Event_OnComplete != null) Event_OnComplete.Invoke(); }

        bool firstStart = true;
        internal virtual void Initialize() { }
        public void Do(int resumeCode)
        {
            
            if (ThreadHandler.IsRunningOnMainThread(Do, resumeCode) == false)
                return;
            try
            {
                MakeForeground();

                if (firstStart && IsOneOffEnabled == true)
                {
                    firstStart = false;
                    MainForm.ResetStepdisplay();
                }

                currentStep = resumeCode;
                CurrentTask = this;

                DoTaskSteps(resumeCode);
            }
            catch (Exception ex)
            {
                Fail(ex);
            }
        }

        protected abstract void DoTaskSteps(int taskStep);

        internal virtual void Fail(Exception e) { MainForm.FailLastStep(e); }
        /// <summary>
        /// Can the task be run on a PI unit?
        /// </summary>
        internal abstract bool IsPiEnabled { get;}
        /// <summary>
        /// Can the task be run by itself?
        /// </summary>
        internal abstract bool IsOneOffEnabled { get; }

        internal delegate void Delegate_OnComplete();
        internal event Delegate_OnComplete Event_OnComplete;

        public void Event_OnComplete_Clear()
        {
            Event_OnComplete = null;
        }

        protected void StartThreadedMethod(bool doNextStepOnComplete, Action action)
        {
            ThreadHandler.StartNewThread(
                (sender, args) =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception ex)
                    {
                        Fail(ex);
                        throw;
                    }
                }
                ,
                (sender, args) => 
                {
                    if (doNextStepOnComplete && args.Error == null)
                        NextStep();
                }
                );
        }

        [DllImport("user32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        protected void MakeForeground()
        {
            SetForegroundWindow(_MainProgram.Handle);
            MainForm.link.TopMost = true;
        }
    }
}
