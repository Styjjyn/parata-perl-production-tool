﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL
{
    public partial class GUI_Prompt_PI_SN : Form
    {

        private string sn;
        internal string ScannedSN
        {
            private set { sn = value; }
            get { return sn; }
        }

        public GUI_Prompt_PI_SN()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ScannedSN = textBox1.Text;
        }
    }
}
