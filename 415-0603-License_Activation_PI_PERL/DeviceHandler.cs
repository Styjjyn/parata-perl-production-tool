﻿using System;
using System.ComponentModel;
using System.Management;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL
{
    internal static class DeviceHandler
    {
        static int timeOut = 30;
        static int delayMillisecs = 500;
        static bool dongleFound = false;
        internal static void FindDongleLoop()
        {
            while (dongleFound == false)
            {
                dongleFound = IsDongleConnected();
                if (dongleFound)
                {
                    timerStarted = false;
                    if (Event_OnDongleFound != null) Event_OnDongleFound();
                    break;
                }

                Thread.Sleep(delayMillisecs);
                AdjustTimer();

            }
        }

        static bool plcFound = false;
        internal static void FindPlcLoop()
        {
            MainForm.StartCountUpProgressBar(timeOut * 1000);
            while (plcFound == false)
            {
                plcFound = IsPlcConnected();
                if (plcFound)
                {

                    timerStarted = false;
                    break;
                }

                Thread.Sleep(delayMillisecs);
                AdjustTimer();
            }
        }

        internal delegate void Delegate_OnDongleFound();
        internal static event Delegate_OnDongleFound Event_OnDongleFound;

        internal delegate void Delegate_OnDongleNotFound();
        internal static event Delegate_OnDongleNotFound Event_OnDongleNotFound;

        static float timeRemaining, a, b;
        static bool timerStarted = false;

        //Adjust the on-screen timer to show the tech how long they have left to connect a dongle to the unit
        private static void AdjustTimer()
        {
            if (!timerStarted)
            {
                timerStarted = true;
                timeRemaining = timeOut;
                return;
            }
            a = delayMillisecs;
            b = 1000.00f;
            timeRemaining -= (a / b);

            if (timeRemaining <= 0)
            {
                throw new Exception("Device was not found in time.");
            }

        }

        //Checks to see if the physical dongle is connected to the PC by looking for a specic vid and pid
        internal static bool IsDongleConnected()
        {
            return IsUsbDeviceConnected("2AF9", "064F");
        }

        internal static bool IsPlcConnected()
        {
            return IsUsbDeviceConnected("2A03","003D");
        }

        private static string usbDongle;
        public static bool IsUsbDeviceConnected(string pid, string vid)
        {
            using (var searcher =
            new ManagementObjectSearcher(@"Select * From Win32_USBControllerDevice"))
            {
                using (var collection = searcher.Get())
                {
                    foreach (var device in collection)
                    {
     
                        usbDongle = Convert.ToString(device);

                        if (usbDongle.Contains(pid) && usbDongle.Contains(vid))
                            return true;
                    }
                }
            }
            return false;
        }

        //Gets the serial number of the connected dongle
        internal static string GetDongleSerialNumber()
        {
            string retString = "Dongle Not Found";
            FindDongleLoop();
            if (dongleFound)
            {
                string[] split = usbDongle.Split('\\', '\\', '\\', '\\');
                retString = split[split.Length - 2];
                int dropZeroes = int.Parse(retString);
                retString = "3-" + dropZeroes + "";
            }

            return retString;
        }
    }
}
