﻿using System;

using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace _415_0603_Production_Tool_PI_PERL
{
    public partial class GUI_TaskStep : UserControl
    {
        public GUI_TaskStep()
        {
            InitializeComponent();
        }

        private void label_Status_Click(object sender, EventArgs e)
        {
            if (label_Status.Text == "FAIL")
                MainForm.ShowMessageBox($"{Task.CurrentTask.TaskName} Failed!", toolTip1.GetToolTip(label_Status));
        }

        private const int EM_GETLINECOUNT = 0xba;
        [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int SendMessage(int hwnd, int wMsg, int wParam, int lParam);


        public GUI_TaskStep(string stepInfo)
        {
            InitializeComponent();
            textBox1.Text = stepInfo;

            Size sz = new Size(textBox1.ClientSize.Width, int.MaxValue);
            TextFormatFlags flags = TextFormatFlags.WordBreak;
            int padding = 3;
            int borders = textBox1.Height - textBox1.ClientSize.Height;
            sz = TextRenderer.MeasureText(textBox1.Text, textBox1.Font, sz, flags);
            int h = sz.Height + borders + padding;
            if (textBox1.Top + h > this.ClientSize.Height - 10)
            {
                h = this.ClientSize.Height - 10 - textBox1.Top;
            }
            textBox1.Height = h;
        }

        public string Tooltip
        {
            set {

                toolTip1.SetToolTip(label_Status, value);
                
            }
        }

        public string Status
        {
            set {
                label_Status.Text = value;


                if (value == "PASS")
                    label_Status.ForeColor = Color.ForestGreen;
                else
                    label_Status.ForeColor = Color.MediumVioletRed;
            }
        }

        public string Detail
        {
            set {
                textBox1.Text = value;
            }
        }
    }
}
