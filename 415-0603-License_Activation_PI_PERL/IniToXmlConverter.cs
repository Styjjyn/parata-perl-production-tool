﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL
{
    static class IniToXmlConverter
    {

        public static void Do()
        {
            DirectoryInfo licensingDirectory = new DirectoryInfo(_CONSTANTS.FOLDER_PATH_DONGLE);
            DirectoryInfo[] dongleFolders = licensingDirectory.GetDirectories("Dongle-*");

            foreach (DirectoryInfo dongleFolder in dongleFolders)
            {
                
                FileInfo dongleInfoFile = dongleFolder.GetFiles(_CONSTANTS.FILE_NAME_DONGLE_INFO)[0];
                DongleInfoFile xmlInfoFile = new DongleInfoFile(dongleInfoFile.Name);
                
                string[] lines = File.ReadAllLines(dongleInfoFile.FullName);

                foreach (string line in lines)
                {
                    if (line.Contains(_CONSTANTS.LINE_ENTRY_BP_CODE))
                    {
                        xmlInfoFile.xml.BP.Code = line.Replace(_CONSTANTS.LINE_ENTRY_BP_CODE, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DATE_PAIRED))
                    {
                        DateTime.TryParse(line.Replace(_CONSTANTS.LINE_ENTRY_DATE_PAIRED, ""), out xmlInfoFile.xml.Perl.dateOfPairing);
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DATE_RECEIVE_BP))
                    {
                        DateTime.TryParse(line.Replace(_CONSTANTS.LINE_ENTRY_DATE_RECEIVE_BP, ""), out xmlInfoFile.xml.BP.dateOfReceipt);
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DATE_RECEIVE_DVC))
                    {
                        DateTime.TryParse(line.Replace(_CONSTANTS.LINE_ENTRY_DATE_RECEIVE_DVC, ""), out xmlInfoFile.xml.DVC.dateOfReceipt);
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DATE_REQUEST_BP))
                    {
                         DateTime.TryParse(line.Replace(_CONSTANTS.LINE_ENTRY_DATE_REQUEST_BP, ""), out xmlInfoFile.xml.BP.dateOfRequest);
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DATE_REQUEST_DVC))
                    {
                        DateTime.TryParse(line.Replace(_CONSTANTS.LINE_ENTRY_DATE_REQUEST_DVC, ""), out xmlInfoFile.xml.DVC.dateOfRequest);
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DONGLE_SN))
                    {
                        xmlInfoFile.xml.SerialNumber = line.Replace(_CONSTANTS.LINE_ENTRY_DONGLE_SN, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DONGLE_TYPE))
                    {
                        xmlInfoFile.xml.LicenseType = line.Replace(_CONSTANTS.LINE_ENTRY_DONGLE_TYPE, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_DVC_CODE))
                    {
                        xmlInfoFile.xml.DVC.Code = line.Replace(_CONSTANTS.LINE_ENTRY_DVC_CODE, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_LOCK_CODE))
                    {
                        xmlInfoFile.xml.DVC.LockCode = line.Replace(_CONSTANTS.LINE_ENTRY_LOCK_CODE, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_MACH_LIC))
                    {
                        xmlInfoFile.xml.BP.Code = line.Replace(_CONSTANTS.LINE_ENTRY_MACH_LIC, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_MO_TYPE))
                    {
                        xmlInfoFile.xml.MoType = line.Replace(_CONSTANTS.LINE_ENTRY_MO_TYPE, "");
                        continue;
                    }

                    if (line.Contains(_CONSTANTS.LINE_ENTRY_PI))
                    {
                        xmlInfoFile.xml.Perl.SN = line.Replace(_CONSTANTS.LINE_ENTRY_PI, "");
                        continue;
                    }

                }

                dongleInfoFile.Delete();
                xmlInfoFile.Save();
            }
        }
    }
}
