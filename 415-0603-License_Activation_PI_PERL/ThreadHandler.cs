﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL
{
    public static class ThreadHandler
    {
        public static Form mainThreadClass;

        static List<BackgroundWorker> activeWorkers = new List<BackgroundWorker>();
        static List<BackgroundWorker> inActiveWorkers = new List<BackgroundWorker>();

        public static void StartNewThread(Action<object, DoWorkEventArgs> threadMethodName)
        {
            BackgroundWorker bgw = null;
            if (inActiveWorkers.Count > 0)
            {
                bgw = inActiveWorkers[inActiveWorkers.Count - 1];
                inActiveWorkers.Remove(bgw);
                activeWorkers.Add(bgw);
                bgw.WorkerSupportsCancellation = true;
                bgw.DoWork += new DoWorkEventHandler(threadMethodName);
                bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(DeactivateThread);
                bgw.RunWorkerAsync();
            }
            else
            {
                bgw = new BackgroundWorker();
                inActiveWorkers.Add(bgw);
                StartNewThread(threadMethodName);
            }
        }

        private static void DeactivateThread(object sender, RunWorkerCompletedEventArgs args)
        {
            activeWorkers.Remove(sender as BackgroundWorker);
        }

        public static bool RequiresInvoke
        {
            get { return mainThreadClass.InvokeRequired; }
        }

        public static void RunActionOnMainThread(Action methodToRun)
        {
            mainThreadClass.Invoke(new MethodInvoker(()=> { methodToRun(); }));
        }

        public static bool RunOnMainThread(Func<bool> methodName)
        {
            bool ret = RequiresInvoke;

            if (mainThreadClass.IsDisposed == false && ret)
            {
                mainThreadClass.Invoke(new MethodInvoker(() => { methodName(); }));
            }

            return !ret;
        }

        static bool programEnding = false;
        public static bool IsRunningOnMainThread(Action methodName)
        {
            bool ret = RequiresInvoke;

            if (!programEnding && ret)
            {
                mainThreadClass.Invoke(new MethodInvoker(() => { methodName(); }));
            }

            return !ret;     
        }

        public static bool IsRunningOnMainThread(Action<int> methodName, int arg)
        {
            bool ret = RequiresInvoke;

            if (!programEnding && ret)
            {
                mainThreadClass.Invoke(new MethodInvoker(() => { methodName(arg); }));
            }

            return !ret;
        }

        public static bool IsRunningOnMainThread(Delegate methodName)
        {
            bool ret = RequiresInvoke;

            if (!programEnding && ret)
            {
                mainThreadClass.Invoke(methodName);
            }

            return !ret;


        }

        public static bool IsRunningOnMainThread(Action<string> methodName)
        {
            bool ret = RequiresInvoke;

            if (!programEnding && ret)
            {
                mainThreadClass.Invoke(methodName);
            }

            return !ret;


        }

        public static bool IsRunningOnMainThread(Action<object, RunWorkerCompletedEventArgs> methodName)
        {
            bool ret = RequiresInvoke;

            if (!programEnding && ret)
            {
                mainThreadClass.Invoke(methodName);
            }

            return !ret;


        }

        delegate void Delegate_NewThreadMethod(object o,DoWorkEventArgs args);
        public static bool IsRunningOnMainThread(Action<object, DoWorkEventArgs> methodName)
        {
            bool ret = RequiresInvoke;

            if (!programEnding && ret)
            {
                mainThreadClass.Invoke(new Delegate_NewThreadMethod(methodName));
            }

            return !ret;


        }

        public static void StopAllThreads()
        {
            programEnding = true;
            foreach (BackgroundWorker bgw in activeWorkers)
            {
                bgw.CancelAsync();
            }
        }

        private static void RemoveThreadFromWorker(BackgroundWorker bgw, Action<object, DoWorkEventArgs> threadMethodName)
        {
            
        }

        internal static void StartNewThread(Action<object, DoWorkEventArgs> methodToRunOnThread, Action<object, RunWorkerCompletedEventArgs> runMethodOnComplete)
        {

            BackgroundWorker bgw = null;
            if (inActiveWorkers.Count > 0)
            {
                bgw = inActiveWorkers[inActiveWorkers.Count - 1];
                bgw = new BackgroundWorker();
                inActiveWorkers.Remove(bgw);
                activeWorkers.Add(bgw);
                bgw.WorkerSupportsCancellation = true;
                bgw.DoWork += new DoWorkEventHandler(methodToRunOnThread);
                bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(runMethodOnComplete);
                bgw.RunWorkerAsync();
            }
            else
            {
                bgw = new BackgroundWorker();
                inActiveWorkers.Add(bgw);
                StartNewThread(methodToRunOnThread, runMethodOnComplete);
            }
        }

        

        delegate void Delegate_OnDoWork();
        static Delegate_OnDoWork Event_OnDoWork;

        static void OnDoWork(object sender, DoWorkEventArgs args)
        {
            if (Event_OnDoWork != null)
            {
                Event_OnDoWork();
            }
        }
    }
}
