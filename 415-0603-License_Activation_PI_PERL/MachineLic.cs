﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL
{
    public class MachineLic
    {
        static string[] lines =
            {
                "PI_LICENSE_FILE",
                "00010001",
                "================================================================",
                "1001",
                "================================================================",
                "####",
                "================================================================"
            };

        string[] myLines;

        public MachineLic(string code)
        {
            myLines = lines;
            myLines[5] = code;
        }

        public void Save(string path)
        {
            File.WriteAllLines(path, myLines,Encoding.UTF8);
        }
    }
}
