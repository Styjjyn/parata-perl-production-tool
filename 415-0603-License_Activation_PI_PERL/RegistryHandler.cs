﻿using Microsoft.Win32;
using System;
using System.Linq;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL
{
    internal static class RegistryHandler
    {

        //[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon]
        static string REGISTRY_HKLM_CONTOLSET_COMPNAME = @"SYSTEM\ControlSet001\Control\ComputerName\ComputerName";
        static string REGISTRY_HKLM_WINLOGON = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon";

        internal static void RestartAppAtStartup(bool restartAppOnStartup)
        {

            //For some reason, the application is running 32bit mode and was using the SOFTWARE\WOW64
            localMachine = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,RegistryView.Registry64);
            string startup = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
            RegistryKey subKey = localMachine.CreateSubKey(startup);

            if (restartAppOnStartup)
            {
                
                try
                {
                    string exePath = $"\"{Application.ExecutablePath}\"";
                    subKey.SetValue("PerlInstallTool", exePath);
                    subKey.Close();
                    localMachine.Close();
                }
                catch (Exception e)
                {
                    MainForm.FailLastStep(e);
                }
            }
            else
            {
                try
                {

                    if(subKey.GetValueNames().Contains("PerlInstallTool"))
                        subKey.DeleteValue("PerlInstallTool");
                }
                catch (Exception e)
                {
                    MainForm.FailLastStep(e);
                }
            }

            
        }
        //"DefaultPassword"="PPYOIF#1ab-01012013!"
        internal static void SetParataUserAsDefault()
        {
            localMachine = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey subKey = localMachine.CreateSubKey(REGISTRY_HKLM_WINLOGON);

            subKey.SetValue("DefaultUserName", "ParataUser");
            subKey.SetValue("DefaultPassword","PPYOIF#1ab-01012013!");
            subKey.Close();
            localMachine.Close();
        }

        static RegistryKey localMachine;
        internal static string PC_Name
        {
            get {
                string retString = "";

                localMachine = Registry.LocalMachine;
                RegistryKey subKey = localMachine.CreateSubKey(REGISTRY_HKLM_CONTOLSET_COMPNAME);
                retString = subKey.GetValue("ComputerName") as string;
                subKey.Close();
                localMachine.Close();
                return retString;
            }
            set {
                //Adjust Controlset001 computer name entries
                localMachine = Registry.LocalMachine;
                RegistryKey subKey = localMachine.CreateSubKey(REGISTRY_HKLM_CONTOLSET_COMPNAME);
                subKey.SetValue("ComputerName", value);
                subKey.Close();

                subKey = localMachine.CreateSubKey(@"SYSTEM\ControlSet001\Services\Tcpip\Parameters");
                subKey.SetValue("NV HostName", value);
                subKey.Close();

                //Adjust CurrentControlSet computer name entries
                subKey = localMachine.CreateSubKey(@"SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName");
                subKey.SetValue("ComputerName", value);
                subKey.Close();

                subKey = localMachine.CreateSubKey(@"SYSTEM\CurrentControlSet\Services\Tcpip\Parameters");
                subKey.SetValue("NV HostName", value);
                subKey.Close();

                //Adjust Parata Systems serial number entry
                subKey = localMachine.CreateSubKey(@"SOFTWARE\Parata Systems\PERL");
                subKey.SetValue("SerialNumber", value);
                subKey.Close();

                localMachine.Close();
            }
            

            
        }
       
    }
}
