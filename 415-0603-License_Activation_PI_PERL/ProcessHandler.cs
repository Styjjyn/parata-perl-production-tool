﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    internal static class ProcessHandler
    {
        internal static void RestartPC()
        {
            StartShutDown("-f -r -t 2");
        }

        static void StartShutDown(string args)
        {
            ProcessStartInfo procInfo = new ProcessStartInfo();
            procInfo.FileName = "cmd";
            procInfo.WindowStyle = ProcessWindowStyle.Hidden;
            procInfo.Arguments = "/C shutdown " + args;
            Process.Start(procInfo);
        }
        

        internal static Process RunElevatedCommandPrompt(string arguments)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "cmd.exe";
            info.Verb = "runas";
            info.Arguments = arguments;
            return Process.Start(info);
        }

        internal static void RunPiSettingsFix()
        {

        }

        internal static void RunPiGUI()
        {

        }

        internal static void CopyFolder(string fromPath, string toPath)
        {

            MainForm.AddStep("Copying folder");
            ProcessStartInfo xcopyInfo = new ProcessStartInfo();
            xcopyInfo.CreateNoWindow = true;
            xcopyInfo.UseShellExecute = false;
            xcopyInfo.FileName = "xcopy.exe";
            xcopyInfo.WindowStyle = ProcessWindowStyle.Hidden;
            xcopyInfo.Arguments = "/C \"" + fromPath + "\" \"" + toPath + "\\\" /Y";

            using (Process xcopyProc = Process.Start(xcopyInfo))
            {

                xcopyProc.WaitForExit();
                if (xcopyProc.ExitCode != 0) throw new Exception("CopyFolder exited with code: " + xcopyProc.ExitCode);
            }
        }

        [DllImport("user32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        //TODO Figure out why the drives wont get removed
        internal static void RemoveNetDrive(string v)
        {
            if (ThreadHandler.IsRunningOnMainThread(RemoveNetDrive) == false)
                return;

            string[] lines = new string[] { "cd /d c:","net use /delete * /Y", "PAUSE"};
            string filePath = $"{_CONSTANTS.FOLDER_PATH_APP}RemDrives.Bat";
            System.IO.File.WriteAllLines(filePath, lines);

            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = $"{filePath}";
            info.WorkingDirectory = "C:";
            info.UseShellExecute = true;

            using (Process p = Process.Start(info))
            {
                p.WaitForExit();
                System.IO.File.Delete(filePath);
                if (p.ExitCode != 0) throw new Exception($"RemoveNetDrive: {v} failed with exitcode: {p.ExitCode}");
            }
 
        }

        internal static void CopyFile(string fromFilePath, string toFilePath)
        {
            ProcessStartInfo xcopyInfo = new ProcessStartInfo();
            xcopyInfo.FileName = "cmd.exe";
            xcopyInfo.Arguments = $"/C echo F|xcopy \"{fromFilePath}\" \"{toFilePath}\" /Y";
            xcopyInfo.CreateNoWindow = true;

            using (Process p = Process.Start(xcopyInfo))
            {
                p.WaitForExit();
                if (p.ExitCode != 0) throw new Exception($"CopyFile command exited with: {p.ExitCode}");
            }
        }
    }
}
