﻿using _415_0603_Production_Tool_PI_PERL;
using _415_0603_Production_Tool_PI_PERL.Tasks;
using _415_0603_Production_Tool_PI_PERL.Tasks._1_Dongle_Request_Activation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL
{
    public partial class MainForm : Form
    {
        internal static MainForm link;
        internal static bool isResuming = false;
        internal static BackgroundWorker BgWorker
        {
            get { return link.backgroundWorker1; }
        }

        public MainForm()
        {
            InitializeComponent();
            link = this;
            ThreadHandler.StartNewThread(S_DriveChecker);
            
        }

        private delegate DialogResult Delegate_GetDialogResult(Form promptToShow);
        internal static DialogResult ShowDialogPrompt(Form promptToShow)
        {
            if (link.InvokeRequired)
            {
                Delegate_GetDialogResult del = new Delegate_GetDialogResult(ShowDialogPrompt);
                return (DialogResult) link.Invoke(del, promptToShow);
            }

            return promptToShow.ShowDialog(link);
        }

        delegate void Delegate_SetText(string text);
        public static void SetClipboardText(string text)
        {

            if (link.InvokeRequired)
            {
                Delegate_SetText del = new Delegate_SetText(SetClipboardText);
                link.Invoke(del, text);
            }
            else
            {

                Clipboard.Clear();
                Clipboard.SetText(text);
            }

        }

        private static void _SetClipBoard(string t, int iter)
        {
            try
            {
                Clipboard.SetText(t);
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                Thread.Sleep(250);
                iter--;
                if (iter > 0)
                    _SetClipBoard(t, iter);
                else
                    throw new Exception("Unable to perform Clipboard Set");
            }
        }

        delegate string Delegate_ReturnString();
        public static string GetClipbardText()
        {
            if (link.InvokeRequired)
            {
                Delegate_ReturnString del = new Delegate_ReturnString(GetClipbardText);
                return (string) link.Invoke(del);
            }
            else

            {
                return Clipboard.GetText();
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// On 'About' menu item selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string info = "Part Number:\t" + _CONSTANTS.APPLICATION_PN + "\r\n" +
                            "App Name:\t" + _CONSTANTS.APPLICATION_NAME + "\r\n" +
                            "App Rev.:\t\t" + _CONSTANTS.APPLICATION_REV + "\r\n";
            MessageBox.Show(this,info,"About this software");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = _CONSTANTS.APPLICATION_PN + " - " + _CONSTANTS.APPLICATION_NAME + " - " + _CONSTANTS.APPLICATION_REV;

            PopulateTaskDropdown();

            if (Configuration_App_Instance.RestartTask != "")
            {
                comboBox_Tasks.Text = Configuration_App_Instance.RestartTask;
                Task.DoTask(Configuration_App_Instance.RestartTask, Configuration_App_Instance.RestartCode);
            }
        }

        private void PopulateTaskDropdown()
        {

            bool isPiMode = Configuration_App_Instance.RunMode == _MainProgram.RunModes.PI;
            
            for (int i = 0; i < Task.Count; i++)
            {
                if (Task.IsOneOffEnabledAtIndex(i) == false)
                    continue;

                if (isPiMode)
                {
                    if (Task.IsTaskPiEnabled(i) == false)
                        continue;
                }
                else
                {
                    if (Task.IsTaskPiEnabled(i) == true)
                        continue;
                }

                comboBox_Tasks.Items.Add(Task.GetTaskName(i));
            }

            
        }

        bool isNetworkConnected;
        private delegate bool Delegate_Bool();
        private bool get_NetworkDriveConnected()
        {
            if (link.InvokeRequired)
            {
                return (bool)link.Invoke(new Delegate_Bool(get_NetworkDriveConnected));
            }
            else
                return isNetworkConnected;
        }

        

        private void set_NetworkDriveConnected(bool b)
        {
            if (link.InvokeRequired)
            {
                try
                {
                    link.Invoke(new MethodInvoker(() => { set_NetworkDriveConnected(b); }));
                }
                catch (ObjectDisposedException)
                {

                }
                
            }
            else
            {
                label_Connection.Visible = !b;
                
                isNetworkConnected = b;

                if(!b) FileHandler.AttemptDriveReconnect("s:");
            }
        }

        private void S_DriveChecker(object sender, DoWorkEventArgs args)
        {

            while (_MainProgram.IsRunning())
            {
                set_NetworkDriveConnected(FileHandler.FolderExists(_CONSTANTS.FOLDER_PATH_PERL));

                    
                Thread.Sleep(1000);
            }
        }

        private void comboBox_Tasks_Click(object sender, EventArgs e)
        {

        }

        private void comboBox_Tasks_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if(comboBox_Tasks.SelectedIndex != -1)
                Task.DoTask(comboBox_Tasks.Text, 0);
        }

        internal static bool TaskChangeAllowed
        {
            set {

                if (link.InvokeRequired)
                {
                    link.Invoke(new MethodInvoker(() => { TaskChangeAllowed = value; }));
                }
                else
                {
                    link.button_tool_GO.Enabled = value ;
                }

            }
        }

        internal static void ResetStepdisplay()
        {
            TaskChangeAllowed = false;
            link.panel_Steps.Controls.Clear();
            link.label_Utility.Text = "";
        }

        delegate void Delegate_AddStep(string dialogStep);
        internal static void AddStep(string dialogStep)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new MethodInvoker(() => { AddStep(dialogStep); }));
            }
            else
            {
                PassLastStep();
                GUI_TaskStep taskStep = new GUI_TaskStep(dialogStep);
                link.panel_Steps.Controls.Add(taskStep);
                link.panel_Steps.ScrollControlIntoView(taskStep);
            }
        }

        internal static void UpdateTimerLabel(int msLeft)
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new MethodInvoker(() => { UpdateTimerLabel(msLeft); }));

            }
            else
            {
                if (msLeft == -1)
                {
                    link.label_Utility.Text = "";
                    return;
                }

                link.label_Utility.Text = "Time Remaining: " + msLeft / 1000;
                link.label_Utility.Size = link.label_Utility.PreferredSize;
            }


        }

        delegate void Delegate_PassLastStep();
        internal static void PassLastStep()
        {
            if (link.InvokeRequired)
            {
                link.Invoke(new MethodInvoker(() => { PassLastStep(); }));

            }
            else
            {
                if (link.panel_Steps.Controls.Count > 0)
                    UpdateLastStep("PASS");
            }

            
        }

        internal static void CompleteTask()
        {
            PassLastStep();
            AddStep("TASK COMPLETE");
            
            PassLastStep();
            link.panel_Steps.ScrollControlIntoView(link.panel_Steps.Controls[link.panel_Steps.Controls.Count-1]);
            TaskChangeAllowed = true;
        }

        internal static void FailLastStep(Exception e)
        {
            FailLastStep(Task.CurrentTask.TaskName,e.ToString());
        }

        internal static void FailLastStep(string s)
        {
            FailLastStep(Task.CurrentTask.TaskName, s);
        }

        public delegate DialogResult Delegate_PromptForDongleLicense();

        internal static DialogResult PromptForDongleLicenseType()
        {
            if (link.InvokeRequired)
            {
                Delegate_PromptForDongleLicense del = new Delegate_PromptForDongleLicense(PromptForDongleLicenseType);
                return (DialogResult)link.Invoke(del);
            }
                
            else
            {
                return new GUI_Prompt_Dongle_LicenseType().ShowDialog(link);
            }
        }

        internal static void FailLastStep(String taskName, String errorInfo)
        {
            TaskChangeAllowed = true;
            if (link.panel_Steps.Controls.Count > 0)
            {
                UpdateLastStep("FAIL", errorInfo);
            }

            ShowMessageBox($"{taskName} Failed",$"Failure Info:\b\r{errorInfo}");
        }

        internal static void UpdateLastStep(string status)
        {
            if (link.InvokeRequired)
                link.Invoke(new MethodInvoker(() => { UpdateLastStep(status); }));
            else
            {
                GUI_TaskStep lastStep = link.panel_Steps.Controls[link.panel_Steps.Controls.Count - 1] as GUI_TaskStep;
                lastStep.Status = status;
            }

            
           

        }

        internal static void UpdateLastStep(string status, string tooltip)
        {
            if (link.InvokeRequired)
                link.Invoke(new MethodInvoker(() => { UpdateLastStep(status, tooltip); }));
            else
            {
                GUI_TaskStep lastStep = link.panel_Steps.Controls[link.panel_Steps.Controls.Count - 1] as GUI_TaskStep;
                lastStep.Status = status;
                lastStep.Tooltip = tooltip;
            }
        }

        internal static void UpdateCurrentStep(string detail)
        {
            GUI_TaskStep currentStep = link.panel_Steps.Controls[link.panel_Steps.Controls.Count - 1] as GUI_TaskStep;
            currentStep.Detail = detail;
        }

        private void panel_Steps_Paint(object sender, PaintEventArgs e)
        {

        }

        delegate string Delegate_ShowSingleEntryPrompt(string caption, string labelText, int entryLength = -1);
        internal static string ShowSingleEntryPrompt(string caption, string labelText, int entryLength = -1)
        {
            if (link.InvokeRequired)
            {
                Delegate_ShowSingleEntryPrompt del = new Delegate_ShowSingleEntryPrompt(ShowSingleEntryPrompt);
                return (string)link.Invoke(del, caption, labelText, entryLength);
            }

            Form form = new Form();
            Label label = new Label();
            TextBox textEntryBox = new TextBox();
            Button proceedButton = new Button();
            TableLayoutPanel layoutPanel = new TableLayoutPanel();

            form.Text = caption;

            label.Text = labelText;

            textEntryBox.Size = new Size(465, 26);
            textEntryBox.TextChanged += new EventHandler((sender, args)=> 
            {
                proceedButton.Enabled = textEntryBox.Text.Length == entryLength;
            });

            proceedButton.Text = "Proceed";
            proceedButton.Enabled = false;
            proceedButton.Anchor = AnchorStyles.Right;
            proceedButton.Click += new EventHandler((sender,args)=> 
            {
                form.DialogResult = DialogResult.Yes;
                form.Close();
            });

            layoutPanel.ColumnCount = 1;
            layoutPanel.RowCount = 3;
            layoutPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            layoutPanel.AutoSize = true;
            layoutPanel.Controls.Add(label);
            layoutPanel.Controls.Add(textEntryBox);
            layoutPanel.Controls.Add(proceedButton);

            form.Controls.Add(layoutPanel);
            form.AutoSize = true;
            form.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            form.ShowDialog(link);

            return textEntryBox.Text;
        }

        delegate DialogResult Delegate_ShowYesNoMessageBox(string caption, string text);
        internal static DialogResult ShowYesNoMessageBox(string caption, string text)
        {
            if (link.InvokeRequired)
            {
                Delegate_ShowYesNoMessageBox del = new Delegate_ShowYesNoMessageBox(ShowYesNoMessageBox);
                return (DialogResult) link.Invoke(del, caption, text);
            }

            return MessageBox.Show(link, text, caption, MessageBoxButtons.YesNo);
        }

        delegate DialogResult Delegate_ShowMessageBox(string caption, string text);
        internal static DialogResult ShowMessageBox(string caption, string text)
        {
            if (link.InvokeRequired)
            {
                Delegate_ShowMessageBox del = new Delegate_ShowMessageBox(ShowMessageBox);
                return (DialogResult)link.Invoke(del, caption, text);
            }

            return MessageBox.Show(link, text, caption);
        }

        delegate void Delegate_SendKeysToMilConfig();
        internal static void SendKeysToMilConfig()
        {
            if (link.InvokeRequired)
            {
                Delegate_SendKeysToMilConfig del = new Delegate_SendKeysToMilConfig(MILConfig.GenerateLockCode);
                link.Invoke(del);
            }
            else
                MILConfig.GenerateLockCode();
        }

        delegate void Delegate_SendKey(string key);
        internal static void SendKey(string key)
        {
            if (link.InvokeRequired)
            {
                Delegate_SendKey del = new Delegate_SendKey(SendKey);
                link.Invoke(del, key);
            }
            else
            {
                SendKeys.Send(key);
            }
        }

        internal static void MILConfigComplete()
        {

        }

        //
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            //int indexToStart;
            //if ((indexToStart = Configuration.RestartIndex) != -1)
            //{
            //    comboBox_Tasks.SelectedIndex = indexToStart;
            //    Task.DoTask(indexToStart, true);
            //}
        }

        private void label_Connection_Click(object sender, EventArgs e)
        {

        }

        public static void StartInfiniteProgressBar()
        {
            if (link.InvokeRequired)
                link.Invoke(new MethodInvoker(StartInfiniteProgressBar));
            else
            {

                link.progressBar1.Style = ProgressBarStyle.Marquee;
                link.progressBar1.Value = 0;
                link.progressBar1.MarqueeAnimationSpeed = 25;

                link.progressBar1.Show();
            }
        }

        public static void StopProgressBar()
        {
            if (link.InvokeRequired)
                link.Invoke(new MethodInvoker(StopProgressBar));
            else
            {
                link.isCounting = false;
                link.progressBar1.Hide();
            }
        }

        public static void StartCountUpProgressBar(int milliSec)
        {
            if (link.isCounting == true)
                return;

            ThreadHandler.StartNewThread(
                (sender, args) => { link.Thread_StartCountUp(milliSec); });
        }

        bool isCounting = false;
        private void Thread_StartCountUp(int millisec)
        {
            isCounting = true;
            SetProgressBarSteps(millisec);
            while (millisec > 0)
            {

                millisec -= 1000;
                Thread.Sleep(1000);
                StepProgressBar();
            }
            StopProgressBar();
        }


        delegate void Delegate_SetInt(int i);
        private void SetProgressBarSteps(int steps)
        {
            if (link.InvokeRequired)
                link.Invoke(new Delegate_SetInt(SetProgressBarSteps), steps);
            else
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
                progressBar1.Maximum = steps;
                progressBar1.Step = 1000;
                progressBar1.Show();
            }
        }

        private void StepProgressBar()
        {
            if (link.InvokeRequired)
                link.Invoke(new MethodInvoker(StepProgressBar));
            else
            {

                progressBar1.PerformStep();
            }

            
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
            int i = link.panel_Steps.Controls.Count - 1;

            if(i >= 0)
                link.panel_Steps.ScrollControlIntoView(link.panel_Steps.Controls[i]);
        }

        private void MainForm_Leave(object sender, EventArgs e)
        {
            int i = link.panel_Steps.Controls.Count - 1;

            if (i >= 0)
                link.panel_Steps.ScrollControlIntoView(link.panel_Steps.Controls[i]);

        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            int i = link.panel_Steps.Controls.Count - 1;

            if (i >= 0)
                link.panel_Steps.ScrollControlIntoView(link.panel_Steps.Controls[i]);


        }


        public static OptionPromptResults ShowOptionPrompt(string caption, string text,string[] options)
        {
            OptionPromptResults results = new OptionPromptResults(options.Length);

            Form form = new Form();
            TableLayoutPanel table = new TableLayoutPanel(), radioButtonPanel = new TableLayoutPanel(), buttonPanel = new TableLayoutPanel();
            Label instructionLabel = new Label();
            Button cancelButton = new Button(), proceedButton = new Button();

            form.Text = caption;
            form.AutoSize = true;
            form.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            form.MaximizeBox = false;
            form.ControlBox = false;

            table.AutoSize = true;
            table.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            table.ColumnCount = 1;
            table.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

            radioButtonPanel.ColumnCount = options.Length;
            radioButtonPanel.RowCount = 1;
            radioButtonPanel.AutoSize = true;
            radioButtonPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            radioButtonPanel.Anchor = AnchorStyles.None;

            instructionLabel.Text = text;
            instructionLabel.Anchor = AnchorStyles.None;
            List<RadioButton> buttonList = new List<RadioButton>();
            for (int i = 0; i < options.Length; i++)
            {
                
                RadioButton rb = new RadioButton();
                buttonList.Add(rb);
                rb.Text = options[i];
                rb.CheckedChanged += (sender, args) => 
                {
                    
                    MessageBox.Show($"RadioButton {buttonList.IndexOf((RadioButton)sender)} attempting to access {results.optionResults.Length}");
                    results.optionResults[buttonList.IndexOf((RadioButton) sender)] = ((RadioButton)sender).Checked; };
                rb.Anchor = AnchorStyles.None;

                radioButtonPanel.Controls.Add(rb);
            }

            buttonPanel.AutoSize = true;
            buttonPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            buttonPanel.ColumnCount = 2;
            buttonPanel.RowCount = 0;
            buttonPanel.Anchor = AnchorStyles.None;

            cancelButton.Text = "Cancel";
            cancelButton.Click += (sender, args)=> { results.dialogResult = DialogResult.Cancel; form.Close(); };
            cancelButton.Anchor = AnchorStyles.Left;
            cancelButton.BackColor = Color.MediumVioletRed;

            proceedButton.Text = "Proceed";
            proceedButton.Click += (sender, args) => {
                results.dialogResult = DialogResult.Yes;
              
                form.Close(); };
            proceedButton.Anchor = AnchorStyles.Right;
            proceedButton.BackColor = Color.LawnGreen;

            buttonPanel.Controls.Add(cancelButton);
            buttonPanel.Controls.Add(proceedButton);

            table.Controls.Add(instructionLabel);
            table.Controls.Add(radioButtonPanel);
            table.Controls.Add(buttonPanel);

            form.Controls.Add(table);
            form.ShowDialog(link);

            return results;
        }

        private void resetAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Configuration_App_Instance.DONGLE_PAIR_STATE = Task.TaskStates.PENDING;
            Configuration_App_Instance.FIN_FUN_PREP_STATE = Task.TaskStates.PENDING;
            Configuration_App_Instance.PC_RENAME_STATE = Task.TaskStates.PENDING;
            Configuration_App_Instance.PLC_UPDATE_STATE = Task.TaskStates.PENDING;
            Configuration_App_Instance.PROMPT_FOR_INPUT_STATE = Task.TaskStates.PENDING;
            Configuration_App_Instance.THINGWORX_STATE = Task.TaskStates.PENDING;
            Configuration_App_Instance.WINDOWS_ACTIVATE_STATE = Task.TaskStates.PENDING;
        }

        private void modifyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form tempForm = new Form();
            tempForm.AutoSize = true;
            tempForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            TableLayoutPanel mainPanel = new TableLayoutPanel();
            mainPanel.ColumnCount = 1;
            mainPanel.AutoSize = true;
            mainPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            mainPanel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
            

            tempForm.Controls.Add(mainPanel);
            tempForm.ShowDialog(this);
        }

        private void AddTaskStateRadioButton(string rbText, Func<Task.TaskStates> getter, Action<Task.TaskStates> setter, ref TableLayoutPanel mainPanel)
        {
            TableLayoutPanel panel = new TableLayoutPanel();
            panel.ColumnCount = 1;
            panel.RowCount = 1;

            RadioButton rb = new RadioButton();
            rb.Text = rbText;
            rb.Checked = getter() == Task.TaskStates.PASS;

            rb.CheckedChanged += new EventHandler((sender, args)=> 
            {
                setter(rb.Checked ? Task.TaskStates.PASS : Task.TaskStates.PENDING);
            });

            panel.Controls.Add(rb);
            mainPanel.Controls.Add(panel);
        }
    }

    public class OptionPromptResults
    {
        public DialogResult dialogResult;
        public bool[] optionResults;

        public OptionPromptResults(int size)
        {
            optionResults = new bool[size];
        }
    }
}
