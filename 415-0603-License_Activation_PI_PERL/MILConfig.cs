﻿using _415_0603_Production_Tool_PI_PERL;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL
{
    internal static class MILConfig
    {


        static bool milConfigOpen = false;
        private static Process process;
        private static IntPtr handle;

        public static string LIC_GEN { get { return "-page LICGEN"; } }
        public static string LIC_ACT { get { return "-page LICACT"; } }

        internal static void Open(string arg)
        {
            if (ThreadHandler.IsRunningOnMainThread(Open) == false)
            {
                return;
            }

            if (!milConfigOpen)
            {

                    ProcessStartInfo info = new ProcessStartInfo(@"c:\\Program Files\\Matrox Imaging\\Tools\\MILConfig.exe", arg);
                    info.WindowStyle = ProcessWindowStyle.Maximized;
                    process = Process.Start(info);

                
            }
            if (arg == LIC_GEN)
                GenerateLockCode();
            else
                ActivateDongle();

            Complete();
        }

        private delegate void Delegate_OnComplete();
        private static event Delegate_OnComplete Event_OnComplete;

        /// <summary>
        /// Opens the MIL Config application with the supplied arguments.
        /// Adds the methodToCallOnComplete to the onComplete event to be called after completion of the Open method
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="methodToCallOnComplete"></param>
        internal static void Open(string arg, Action methodToCallOnComplete)
        {
            Delegate_OnComplete del = new Delegate_OnComplete(methodToCallOnComplete);
            Delegate_OnComplete delRem = new Delegate_OnComplete(()=> { Event_OnComplete -= del; });
            Event_OnComplete += del;
            Event_OnComplete += delRem;

            Open(arg);
        }

        internal static void GenerateLockCode()
        {
            handle = process.MainWindowHandle;
            
            MainForm.link.TopMost = false;
            Thread.Sleep(1000);
            SetForegroundWindow(handle);
            SendKeys.Send("{TAB}");
            SendKeys.Send("{TAB}");
            //Use test mode if you are using a dongle that is already activated
            if (_MainProgram.QAMode == false)
            {
                SendKeys.Send("{TAB}");
                SendKeys.Send("{DOWN}");
                SendKeys.Send("{TAB}");
                SendKeys.Send("{TAB}");
                SendKeys.Send("{TAB}");
                SendKeys.Send("{ENTER}");
                SendKeys.Send("{ENTER}");
            }

            Thread.Sleep(500);
            SendKeys.SendWait("^{c}");
            Thread.Sleep(250);
            SetForegroundWindow(_MainProgram.Handle);
            _Variables.LockCode = Clipboard.GetText();
            process.CloseMainWindow();

            if (_MainProgram.QAMode == true)
            {
                //Verify that the lock code read is what is generated
                MessageBox.Show("LockCode= " + _Variables.LockCode);
            }

            MainForm.MILConfigComplete();
        }

        internal static void ActivateDongle()
        {
            handle = process.MainWindowHandle;

            //Clipboard.SetText(_Variables.DvcCode);
            MainForm.link.TopMost = false;
            Thread.Sleep(3000);
            SetForegroundWindow(handle);
            SendKeys.Send("{TAB}");
            Thread.Sleep(250);
            SendKeys.Send("^{v}");
            Thread.Sleep(250);
            SendKeys.Send("{TAB}");
            Thread.Sleep(250);
            SendKeys.Send("{ENTER}");
            Thread.Sleep(2000);
            SendKeys.SendWait("^{c}");
            Thread.Sleep(250);
            SetForegroundWindow(_MainProgram.Handle);
            _Variables.LockCode = Clipboard.GetText();
            Thread.Sleep(1000);
            process.CloseMainWindow();
        }

        private static void Complete()
        {
            if (Event_OnComplete != null)
            {
                Event_OnComplete();
            }
        }

        internal static void Close()
        {
            try
            {
                process.Close();
            }
            catch (Exception e)
            {
                ErrorHandler.AddError(ErrorHandler.Errors.CLOSING_MIL_CONFIG_ERROR, e.ToString());
            }

        }

        [DllImport("user32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        internal static void TakeScreenShot()
        {
            //Minimize this application window, wait 1 second, Save screenshot to folder, remaximize this application window, close MIL Config 
            _MainProgram.WindowState = FormWindowState.Minimized;
            Thread.Sleep(1000);
            FileHandler.SaveScreenshot();
            _MainProgram.WindowState = FormWindowState.Normal;
        }

    }

    
}
