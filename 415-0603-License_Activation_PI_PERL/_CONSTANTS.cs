﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using _415_0603_Production_Tool_PI_PERL;

using System.Windows.Forms;


namespace _415_0603_Production_Tool_PI_PERL
{
    /// <summary>
    /// This class provides access to hardcoded or config file based information
    /// </summary>
    public static class _CONSTANTS
    {
        //Property Template
        //
        //static type variable_Name = "0.0.1 BETA";
        //internal static type VARIABLE_NAME { get { return variable_Name; } }
        //
        #region Application Constants

        static string application_Text = APPLICATION_PN + "-" + APPLICATION_NAME;
        public static string APPLICATION_TEXT { get { return APPLICATION_PN + "-" + APPLICATION_NAME; } }

        static string application_Name = "Perl Production Tool";
        internal static string APPLICATION_NAME { get { return application_Name; } }
        static string application_PN = "415-0603";
        internal static string APPLICATION_PN { get { return application_PN; } }
        static string application_Rev = "1.0.1";
        internal static string APPLICATION_REV { get { return application_Rev; } }
        internal static string APPLICATION_NAME_RECEIVER = "Perl-PI-Dongle Activation Receiver";
        internal static string APPLICATION_NAME_REQUESTOR = "Perl-PI-Dongle Activation Request";
        internal static string APPLICATION_VERSION = "0.1";
        #endregion

        #region Folder Constants
        internal static string FOLDER_PATH_PERL = "\\\\fserver\\Shared\\Manufacturing\\Production\\!Private\\Perl\\";
        internal static string FOLDER_PATH_UNIT_FOLDER = $"{FOLDER_PATH_PERL}Units\\";
        internal static string FOLDER_PREFIX_DONGLE = "Dongle-";
        internal static string FOLDER_PATH_DONGLE = FOLDER_PATH_PERL + "Licensing\\";
        internal static string FOLDER_PATH_APP = (@"c:\Parata Systems\" + APPLICATION_PN + "-" + APPLICATION_NAME + "\\");
        internal static string FOLDER_PATH_APP_WORKING = FOLDER_PATH_APP + "Working\\";
        internal static string FOLDER_PATH_PI_MAIN = @"C:\Program Files\bba5402d-029c-4858-8b52-57b472251bc4";
        internal static string FOLDER_PATH_FINAL_FUNCTIONAL { get{ return $"{FOLDER_PATH_PERL}Final_Functional\\"; } }
        internal static string FOLDER_PATH_THINGWORX { get { return $"{FOLDER_PATH_FINAL_FUNCTIONAL}Thingworx\\"; } }
        internal static string FOLDER_PATH_SPEED_CONFIGS { get { return $"{FOLDER_PATH_FINAL_FUNCTIONAL}SpeedConfigurations\\"; } }
        internal static string FOLDER_PATH_APP_TEMPLATE { get { return FOLDER_PATH_PERL + APPLICATION_TEXT + "\\Templates\\"; } }
        #endregion

        #region File Constants
        internal static string FILE_NAME_DONGLE_INFO = "DVC Activation Info.txt";
        internal static string FILE_NAME_SCREENSHOT_MILCONFIG = "MIL Config.jpeg";
        internal static string FILE_NAME_APP_CFG = APPLICATION_PN + ".Configuration.xml";
        internal static string FILE_NAME_TEMPLATE_MACH_LIC { get { return FOLDER_PATH_APP_TEMPLATE + "TEMPLATE_Machine.Lic"; } }
        internal static readonly string FILE_NAME_ARDUINO = @"C:\Install\PLC Software\arduino-1.8.2\arduino.exe";
        internal static readonly string FILE_NAME_EMAIL_CFG = $@"{Application.StartupPath}\Configuration.xml";
        public static string FOLDER_PATH_ARDUINO { get { return FOLDER_PATH_PERL + "Final_Functional\\Arduino"; } }
        internal static string FILE_NAME_PERL1_CONFIG { get { return FOLDER_PATH_SPEED_CONFIGS + "ExportSettings_901-0172.xml"; } }
        internal static string FILE_NAME_THINGWORX_SESSION_VARS = @"C:\Parata\Install\Install Thingworx_IM\DefaultSessionVariablesSaveFile.txt";
        #endregion

        #region DVC Email Constants
        internal static readonly string EMAIL_RECIPIENT_DVC = "alex@machinevision.nl";
        internal static readonly string EMAIL_SUBJECT_DVC = "Perl-Pi-Dongle D### Activation Code Request";
        internal static readonly string EMAIL_CC_DVC = "support@machinevision.nl;" +
                                                            "RKotecha@parata.com;" +
                                                            "ATatum@parata.com;" +
                                                            "JBrooks@parata.com";
        internal static readonly string EMAIL_BODY_DVC = "<p>Stick Code: D### <br/> Lock Code= L###</p>";
        #endregion

        #region BP Email Constants
        internal static readonly string EMAIL_RECIPIENT_BP = "gijsvanschelven@blisterpartner.nl";       
        internal static readonly string EMAIL_SUBJECT_BP = "Perl-Pi-Dongle D### Machine.lic File Request";
        internal static readonly string EMAIL_CC_BP = "RKotecha@parata.com;" +
                                                            "ATatum@parata.com;" +
                                                            "JBrooks@parata.com";
        internal static readonly string EMAIL_BODY_BP = "<p>Dongle SN: D###</p><p>License Type: LIC#</p>";

        #endregion

        #region Line Entry Constants
        internal static readonly string LINE_ENTRY_MACH_LIC = "#LIC#";
        internal static readonly string LINE_ENTRY_GROUP_END = "";
        internal static readonly string LINE_ENTRY_GROUP_START_CODES = "[Codes and Serials]";
        internal static readonly string LINE_ENTRY_DONGLE_SN = "Stick Code= ";
        internal static readonly string LINE_ENTRY_DONGLE_TYPE = "License Type= ";
        internal static readonly string LINE_ENTRY_LOCK_CODE = "Lock Code= ";
        internal static readonly string LINE_ENTRY_DVC_CODE = "DVC Activation Code= ";
        internal static readonly string LINE_ENTRY_BP_CODE = "BP Machine.Lic Code= ";
        internal static readonly string LINE_ENTRY_PI = "Paired PI SN= ";
        internal static readonly string LINE_ENTRY_MO_TYPE = "Usage Type= ";

        internal static readonly string LINE_ENTRY_GROUP_START_DATES = "[Dates]";
        internal static readonly string LINE_ENTRY_DATE_REQUEST_DVC = "Date of DVC Code Request= ";
        internal static readonly string LINE_ENTRY_DATE_REQUEST_BP = "Date of BP Code Request= ";
        internal static readonly string LINE_ENTRY_DATE_RECEIVE_DVC = "Date of DVC Code Receipt= ";
        internal static readonly string LINE_ENTRY_DATE_RECEIVE_BP = "Date of BP Code Receipt= ";
        internal static readonly string LINE_ENTRY_DATE_PAIRED = "Date of PI Pair= ";
        internal static readonly string CONFIG_GROUP_LINE_ENTRY_APP = "[Application]";
        internal static readonly string CONFIG_LINE_ENTRY_APP_VERSION = "ApplicationVersion";
        internal static readonly string CONFIG_LINE_ENTRY_RESTART_INDEX = "TaskRestartName";
        internal static readonly string CONFIG_LINE_ENTRY_RESTART_CODE = "TaskRestartCode";
        #endregion

        internal static readonly string LICENSE_PI_ONLY = "PI ONLY";
        internal static readonly string LICENSE_PI_CR = "PI+C&R";

        internal static readonly int LENGTH_LOCK_CODE = 71;
        internal static readonly int LENGTH_ACTIVATION_CODE = 94;
        internal static readonly int LENGTH_BP_CODE = 64;
        internal static readonly string FILE_NAME_WINDOWS_LICENSE = "WIN 10 Lic.txt";
        internal static readonly string CONFIG_GROUP_LINE_ENTRY_TASKS = "Tasks";
        internal static readonly string CONFIG_LINE_ENTRY_TASK_PC_RENAME = "Pc_Rename_Progress";
        internal static readonly string CONFIG_LINE_ENTRY_TASK_DONGLE_PAIR = "Dongle_Pair_Progress";
        internal static readonly string CONFIG_LINE_ENTRY_TASK_PLC_UPDATE = "PLC_Update_Progress";
        internal static readonly string CONFIG_LINE_ENTRY_TASK_THINGWORX = "Thingworx_Progress";
        internal static readonly string CONFIG_LINE_ENTRY_TASK_WINDOWS_ACTIVATE = "Windows_Activate_Progress";
        internal static readonly XName CONFIG_LINE_ENTRY_RUN_MODE = "Run_Mode";
        

        internal static string FOLDER_PATH_TEST_BATCH { get { return $"{FOLDER_PATH_FINAL_FUNCTIONAL}Test_Batch\\"; } }
        internal static string FOLDER_PATH_BATCH_MASTER { get { return $"{FOLDER_PATH_TEST_BATCH}MASTER"; } }

        public static string FILE_NAME_PI_CONFIG_EXPORT { get { return @"C:\Pi\Temp\ExportSettings_All.xml"; } }
    }
}
