﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static _415_0603_Production_Tool_PI_PERL.Configuration_App_Global.Settings.cDonglesPendingActivation;

namespace _415_0603_Production_Tool_PI_PERL
{
    public  class Configuration_App_Global
    {
        private static Configuration_App_Global link;
        public enum SettingTarget
        {
            DVC_TO,
            DVC_CC,
            DVC_SUBJECT,
            DVC_BODY,
            DVC_REPLY_TO,
            BP_TO,
            BP_CC,
            BP_SUBJECT,
            BP_BODY,
            BP_REPLY_TO
        }
        public Settings settings;

        [XmlRoot]
        public class Settings
        {
            public cDonglesPendingActivation DonglesPendingActivation = new cDonglesPendingActivation();

            public class cDonglesPendingActivation
            {
                public List<Dongle> pending = new List<Dongle>();

                public class Dongle
                {
                    public string DongleId = "";
                    public string filePath = "";
                    public string LockCode = "";
                    public bool DvcReceived = false;
                    public bool BpReceived = false;
                }

                public void Add(Dongle dongle, string filePath)
                {
                    pending.Add(dongle);
                }

                public void RemoveAt(int i)
                {
                    pending.RemoveAt(i);
                }

                public Dongle this[int i]
                {
                    get {
                        return pending[i];
                    }
                }

                internal List<Dongle> DonglesNeedingDvcActivation()
                {
                    List<Dongle> needDvc = pending.Where((d) => d.DvcReceived == false).ToList<Dongle>();
                    return needDvc; 
                }

                internal List<Dongle> DonglesNeedingBpActivation()
                {
                    List<Dongle> needDvc = pending.Where((d) => d.BpReceived == false).ToList<Dongle>();
                    return needDvc;
                }

            }

            public EmailSettings DvcEmail, BpEmail;

            public static Settings Load()
            {
                Settings ret;

                if (File.Exists(_CONSTANTS.FILE_NAME_EMAIL_CFG) == false)
                {
                    ret = new Settings();
                    ret.Save();
                    return ret;
                }

                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                using (FileStream stream = new FileStream(_CONSTANTS.FILE_NAME_EMAIL_CFG, FileMode.Open))
                {
                    ret = serializer.Deserialize(stream) as Settings;
                    stream.Close();
                }

                return ret;
            }

            public void Save()
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                using (FileStream stream = new FileStream(_CONSTANTS.FILE_NAME_EMAIL_CFG, FileMode.Create))
                {
                    serializer.Serialize(stream, this);
                    stream.Close();
                }
            }

            public Settings()
            {
                DvcEmail = new EmailSettings()
                    .To("alex@machinevision.nl")
                    .CC("support@machinevision.nl;RKotecha@parata.com;ATatum@parata.com;JBrooks@parata.com")
                    .Subject("Perl-Pi-Dongle D### Activation Code Request")
                    .HtmlBody("<p>Stick Code: D### <br/> Lock Code= L###</p>")
                    .ReplyTo("ATatum@parata.com;JBrooks@parata.com");

                BpEmail = new EmailSettings()
                    .To("gijsvanschelven@blisterpartner.nl")
                    .CC("RKotecha@parata.com;ATatum@parata.com;JBrooks@parata.com")
                    .Subject("Perl-Pi-Dongle D### Machine.lic File Request")
                    .HtmlBody("<p>Dongle SN: D###</p><p>License Type: LIC#</p>")
                    .ReplyTo("ATatum@parata.com;JBrooks@parata.com");
            }

            public class EmailSettings
            {
                
                public string emailRecipients;
                public string ccRecipients;
                public string subject;
                public string htmlBody;
                public string replyRecipients;

                public EmailSettings To(string s)
                {
                    emailRecipients = s;
                    return this;
                }

                public EmailSettings CC(string s)
                {
                    ccRecipients = s;
                    return this;
                }

                public EmailSettings Subject(string s)
                {
                    subject = s;
                    return this;
                }

                public EmailSettings HtmlBody(string s)
                {
                    htmlBody = s;
                    return this;
                }

                public EmailSettings ReplyTo(string s)
                {
                    replyRecipients = s;
                    return this;
                }
            }

        }

        internal static List<Dongle> GetDonglePathsNeedingDvcCode()
        {
            return link.settings.DonglesPendingActivation.DonglesNeedingDvcActivation();
        }

        internal static string GetValue(SettingTarget target)
        {
            switch (target)
            {
                case SettingTarget.DVC_TO:
                    return link.settings.DvcEmail.emailRecipients;
                case SettingTarget.DVC_CC:
                    return link.settings.DvcEmail.ccRecipients;
                case SettingTarget.DVC_SUBJECT:
                    return link.settings.DvcEmail.subject;
                case SettingTarget.DVC_BODY:
                    return link.settings.DvcEmail.htmlBody;
                case SettingTarget.DVC_REPLY_TO:
                    return link.settings.DvcEmail.replyRecipients;
                case SettingTarget.BP_TO:
                    return link.settings.BpEmail.emailRecipients;
                case SettingTarget.BP_CC:
                    return link.settings.BpEmail.ccRecipients;
                case SettingTarget.BP_SUBJECT:
                    return link.settings.BpEmail.subject;
                case SettingTarget.BP_BODY:
                    return link.settings.BpEmail.htmlBody;
                case SettingTarget.BP_REPLY_TO:
                    return link.settings.BpEmail.replyRecipients;
                default:
                    return $"{target.ToString()} NOT FOUND";
            }
        }


        internal Configuration_App_Global()
        {
            if (link == null)
                link = this;

            settings = Settings.Load();
        }

        internal void ReceiveDvcCodeFor(Dongle dongle)
        {

            Dongle temp = settings.DonglesPendingActivation.pending.Where((d) => d.DongleId == dongle.DongleId).FirstOrDefault();

            if (temp == null)
            {
                MainForm.ShowMessageBox("Error", $"{dongle.DongleId} not found in pending list");
                return;
            }

            temp.DvcReceived = true;

            if (temp.BpReceived)
            {
                settings.DonglesPendingActivation.pending.Remove(temp);
            }
            settings.Save();
        }
    }
}
