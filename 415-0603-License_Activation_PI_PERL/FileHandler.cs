﻿
using _415_0603_Production_Tool_PI_PERL.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Collections;

namespace _415_0603_Production_Tool_PI_PERL
{
    public static class FileHandler
    {

        
        internal static void WriteInfoToFile(string filePath, Dictionary<string, string> configSettings)
        {
            string[] keys = configSettings.Keys.ToArray();

            for (int i = 0; i < keys.Length; i++)
            {
                if (configSettings[keys[i]] != null)
                    keys[i] += configSettings[keys[i]];
            }
            File.WriteAllLines(filePath, keys);
        }

       
        internal static void CreateMfgFolderForSalesforce()
        {
            string piSn = Configuration_App_Instance.PiSn;
            string fromPath = $@"{_CONSTANTS.FOLDER_PATH_UNIT_FOLDER}{piSn}";
            string zipPath = $"{_CONSTANTS.FOLDER_PATH_APP}{piSn} MFG Files.zip";
            string newPath = $@"{fromPath}\{piSn} MFG Files.zip"; 

            if (File.Exists(zipPath))
                File.Delete(zipPath);

            ZipFile.CreateFromDirectory(fromPath, zipPath);

            ProcessHandler.CopyFile(zipPath, newPath);

            File.Delete(zipPath);
        }

        internal static void EnsureAppConfigExists()
        {
            throw new NotImplementedException();
        }

        internal static void EnsureFileExists(string filePath, Dictionary<string, string> configSettings)
        {

            if (File.Exists(filePath) == false)
            {
                WriteInfoToFile(filePath,configSettings);
            }
            
        }

        internal static void GetValuesFromFile(string fileName, ref Dictionary<string, string> configSettings)
        {
            List<string> keys = configSettings.Keys.ToList();
            List<int> indecesOfValuesFound = new List<int>();
            string[] linesRead = File.ReadAllLines(fileName);
            string currentLine = "";
            for (int i = 0; i < linesRead.Length; i++)
            {
                currentLine = linesRead[i];
                for (int j = 0; j < keys.Count; j++)
                {
                    if (indecesOfValuesFound.Contains(j) == false && currentLine.Contains(keys[j]))
                    {
                        indecesOfValuesFound.Add(j);
                        currentLine = currentLine.Replace(keys[j],"");
                        configSettings[keys[j]] = currentLine;
                    }
                } 
            }
        }

        internal static void AddWinLicFileToPiFolder()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(_CONSTANTS.FOLDER_PATH_UNIT_FOLDER);
            if (dirInfo.Exists == false) throw new Exception($"Cannot locate: {dirInfo.FullName}");
            //Found PERL folder on shared drive

            DirectoryInfo[] dirs = dirInfo.GetDirectories(Configuration_App_Instance.PiSn);
            if (dirs.Length == 0) throw new Exception($"Cannot locate\r\n{Configuration_App_Instance.PiSn}\r\nAt folder location:\r\n{dirInfo.FullName}");
            //Found PI Fodler in PERL folder

            FileInfo[] files = dirs[0].GetFiles(_CONSTANTS.FILE_NAME_WINDOWS_LICENSE);
            if (files.Length != 0)
            {
               //User accepted file overwrite
                files[0].Delete();
                
            }

            string[] lines = { Configuration_App_Instance.WindowsKey };
            File.WriteAllLines($"{dirs[0].FullName}\\{_CONSTANTS.FILE_NAME_WINDOWS_LICENSE}", lines);
        }

        static string[] linesRead;
        

        private static void EnsureFolderExists(string folderPath)
        {
            
            Directory.CreateDirectory(folderPath);
        }

        public static void EnsureAppFolderExists()
        {
            EnsureFolderExists(_CONSTANTS.FOLDER_PATH_APP);

        }

        internal static void EnsureDongleFolderExists()
        {
            EnsureFolderExists(FullDongleFolderPath);
        }

        internal static void AddActivationCodesFromEmail(List<string> lockCodeList, List<string> swLicCodeList)
        {
            //Where the dongle folders are located on the shared drive
            string licensePath = _CONSTANTS.FOLDER_PATH_DONGLE;


            //All of the dongle folders present in the path above
            string[] dongleFolders = Directory.GetDirectories(licensePath);

            //Iterate through each folder
            foreach (string dongleFolder in dongleFolders)
            {
                if (dongleFolder.Contains(_CONSTANTS.FOLDER_PREFIX_DONGLE) == false)
                    continue;

                //Get all the dongle info files from this folder (there should be only 1, if not, the first found is used)
                string[] fileNames = Directory.GetFiles(dongleFolder, _CONSTANTS.FILE_NAME_DONGLE_INFO);

                //Check to see at least one dongle file exists, error out if not
                if (fileNames.Length == 0)
                    throw new Exception(_CONSTANTS.FILE_NAME_DONGLE_INFO + " does not exist in folder: " + dongleFolder);


                DongleInfoFile dongleFile = DongleInfoFile.Load(dongleFolder.Replace(licensePath,"").Replace(_CONSTANTS.FOLDER_PREFIX_DONGLE,""));
               
                int indexOfLockcode = lockCodeList.IndexOf(dongleFile.xml.DVC.LockCode);

                if (indexOfLockcode == -1)
                {
                   
                    continue;
                }

      
                dongleFile.xml.DVC.Code = swLicCodeList[indexOfLockcode];
                dongleFile.xml.DVC.dateOfReceipt = DateTime.Now;
                dongleFile.Save();
                //TODO Complete the 'PendingDongleActivations' logic
                //_MainProgram.emailConfig.ReceiveDvcCodeFor(dongleFile.xml.SerialNumber);


            }
            //DONE
        }

        internal static void CopyDongleFolderToPiFolder()
        {
            string piFolderPath = _CONSTANTS.FOLDER_PATH_UNIT_FOLDER + Configuration_App_Instance.PiSn + "\\" + _CONSTANTS.FOLDER_PREFIX_DONGLE + _Variables.DongleSN;
            string dongleFolderPath = FullDongleFolderPath;

            ProcessHandler.CopyFolder(dongleFolderPath, piFolderPath);
        }

        internal static void CreateMachineLicenseFile()
        {

            if (ThreadHandler.IsRunningOnMainThread(CreateMachineLicenseFile))
            {
                linesRead = File.ReadAllLines(_CONSTANTS.FILE_NAME_TEMPLATE_MACH_LIC);

                for (int i = 0; i < linesRead.Length; i++)
                {
                    if (linesRead[i].Contains(_CONSTANTS.LINE_ENTRY_MACH_LIC))
                    {
                        linesRead[i] = _Variables.BpCode;
                    }
                }

                string fromFilePath = _CONSTANTS.FOLDER_PATH_APP_TEMPLATE + "Machine.Lic";
                File.WriteAllLines(fromFilePath, linesRead);

                string toFilePath = FullDongleFolderPath + "\\Machine.Lic";

                ProcessHandler.CopyFile(fromFilePath, toFilePath);

                toFilePath = _CONSTANTS.FOLDER_PATH_PI_MAIN + "\\Machine.Lic";
                ProcessHandler.CopyFile(fromFilePath, toFilePath);
            }
            
        }

        internal static string FullDongleFolderPath
        {
            get {
                return _CONSTANTS.FOLDER_PATH_DONGLE + _CONSTANTS.FOLDER_PREFIX_DONGLE + _Variables.DongleSN;
            }
        }

        static float timeOut = 10000, timeLeft;
        public static string PlcUpdateFilePath {
            get {
                timeLeft = timeOut;
                string path = "NULL";
                string startPath = _CONSTANTS.FOLDER_PATH_ARDUINO;

                if (ContainsFile(startPath, "PiPlc.ino", ref path) == false)
                    throw new Exception("PiPlc.ino was not found anywhere in: " + startPath);

                return path;
            }
        }

        public static string ThingworxInstallerExe {
            get {
                DirectoryInfo dir = new DirectoryInfo(_CONSTANTS.FOLDER_PATH_THINGWORX);
                FileInfo[] allFiles = dir.GetFiles("PerlThingworx*.exe");
                if (allFiles.Length > 0)
                    return allFiles[0].FullName;

                throw new Exception("Could not locate PerlThingworx.exe in " + dir.FullName);
            }
        }

        private static void AdjustTimer(int amt)
        {
            timeLeft += amt;
            if (timeLeft <= 0)
                throw new Exception("File not found in time.");

        }

        private static bool ContainsFile(string path, string searchFor,ref string pathFound)
        {
            AdjustTimer(-10);
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            FileInfo[] allFiles = dirInfo.GetFiles(searchFor);
            if (allFiles.Length > 0)
            {
                pathFound = allFiles[0].FullName;
                return true;
            }

            DirectoryInfo[] allDirs = dirInfo.GetDirectories();
            foreach (DirectoryInfo dir in allDirs)
            {
                if (ContainsFile(dir.FullName, searchFor, ref pathFound)) return true;
            }
            return false;
        }

        internal static bool FolderExists(string folderPath)
        {
            return Directory.Exists(folderPath);
        }





        internal static string[] GetDirectories(string inPath)
        {
            return Directory.GetDirectories(inPath);
        }

        internal static bool DongleFolderExists()
        {
            bool exists = false;

            exists = Directory.Exists(_CONSTANTS.FOLDER_PREFIX_DONGLE + _Variables.DongleSN);

            return exists;
        }

        internal static void CreateDongleFolder()
        {
            CreateFolder(_CONSTANTS.FOLDER_PATH_DONGLE + _CONSTANTS.FOLDER_PREFIX_DONGLE + _Variables.DongleSN);
        }

        internal static void CreateDongleInfoFile()
        {
            string dongleFile = _CONSTANTS.FOLDER_PATH_DONGLE + _CONSTANTS.FOLDER_PREFIX_DONGLE + _Variables.DongleSN + "\\" + _CONSTANTS.FILE_NAME_DONGLE_INFO;
            string[] linesToWrite = {
                    _CONSTANTS.LINE_ENTRY_GROUP_START_CODES,
                    _CONSTANTS.LINE_ENTRY_DONGLE_SN,
                    _CONSTANTS.LINE_ENTRY_DONGLE_TYPE,
                    _CONSTANTS.LINE_ENTRY_LOCK_CODE,
                    _CONSTANTS.LINE_ENTRY_DVC_CODE,
                    _CONSTANTS.LINE_ENTRY_BP_CODE,
                    _CONSTANTS.LINE_ENTRY_PI,
                    _CONSTANTS.LINE_ENTRY_GROUP_END,

                    _CONSTANTS.LINE_ENTRY_GROUP_START_DATES,
                    _CONSTANTS.LINE_ENTRY_DATE_REQUEST_DVC,
                    _CONSTANTS.LINE_ENTRY_DATE_REQUEST_BP,
                    _CONSTANTS.LINE_ENTRY_DATE_RECEIVE_DVC,
                    _CONSTANTS.LINE_ENTRY_DATE_RECEIVE_BP,
                    _CONSTANTS.LINE_ENTRY_DATE_PAIRED,
                    _CONSTANTS.LINE_ENTRY_GROUP_END
                };
            if (File.Exists(dongleFile) == false)
            {
                File.WriteAllLines(dongleFile, linesToWrite);
                
            }
            else
            {
                if (MainForm.ShowYesNoMessageBox("Overwrite file?", "There is an existing '" + _CONSTANTS.FILE_NAME_DONGLE_INFO + "' for Dongle-" + _Variables.DongleSN + ". \r\nWould you like to completely overwrite this file?") == DialogResult.Yes)
                {
                    File.WriteAllLines(dongleFile, linesToWrite);
                }
                else
                    throw new Exception("Existing File Found: " + dongleFile + ".\b\rUser denied overwrite.");
            }
        }

        internal static void CreatePIFolder()
        {
            CreateFolder(_CONSTANTS.FOLDER_PATH_UNIT_FOLDER + _Variables.PISN);
        }

        private static void CreateFolder(string path)
        {
            Directory.CreateDirectory(path);
        }

        internal static void SaveScreenshot()
        {
            string ssPath = _CONSTANTS.FOLDER_PATH_DONGLE + _CONSTANTS.FOLDER_PREFIX_DONGLE + _Variables.DongleSN +"\\" + _CONSTANTS.FILE_NAME_SCREENSHOT_MILCONFIG;

            Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            Graphics graphics = Graphics.FromImage(bitmap as Image);

            graphics.CopyFromScreen(0, 0, 0, 0, bitmap.Size);

            bitmap.Save(ssPath, ImageFormat.Jpeg);

        }

        private static void MoveFolder(string fromPath, string toPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(fromPath);
            dirInfo.MoveTo(toPath);
        }

        internal static void AddMachineLicCodesFromEmail(Dictionary<string, Outlook.Attachment> info)
        {
            //Get list of all dongle folders
            string[] dongleFolders = Directory.GetDirectories(_CONSTANTS.FOLDER_PATH_DONGLE);

            string dongleSN = "";
            string attachmentFileName = "";
            string[] linesReadFromAttachmentFile;
            string bPCode = "";
            bool fileExists = false;

            Outlook.Attachment attachment = null;
            //foreach dongle folder
            foreach (string dongleFolderName in dongleFolders)
            {
                dongleSN = dongleFolderName.Replace(_CONSTANTS.FOLDER_PATH_DONGLE,"").Replace(_CONSTANTS.FOLDER_PREFIX_DONGLE,"");

                if (info.TryGetValue(dongleSN, out attachment))
                {
                    attachmentFileName = dongleFolderName + "\\" + attachment.FileName;
                    fileExists = File.Exists(attachmentFileName);

                    if (fileExists)
                    {
                        if (MainForm.ShowYesNoMessageBox("Overwrite file?", "Do you want to overwrite the file\r\n" + attachmentFileName) == DialogResult.Yes)
                        {
                            MainForm.AddStep("Replacing " + dongleSN + "'s preexisting" + _CONSTANTS.FILE_NAME_DONGLE_INFO + " with new one found in email.");
                            File.Move(attachmentFileName, attachmentFileName + ".OLD");
                        }
                        else
                        {
                            continue;
                        }
                        
                    }

                    MainForm.AddStep("Searching for machine.lic code in\r\n"+attachmentFileName);
                    attachment.SaveAsFile(attachmentFileName);
                    linesReadFromAttachmentFile = File.ReadAllLines(attachmentFileName);

                    foreach (string line in linesReadFromAttachmentFile)
                    {
                        if (line.Contains("=") == false && line.Length == _CONSTANTS.LENGTH_BP_CODE)
                        {
                            bPCode = line;
                            break;
                        }
                    }

                    if (bPCode.Length != _CONSTANTS.LENGTH_BP_CODE)
                        throw new Exception("The machine license code read:\r\n" + bPCode + "\r\nIs incorrect.");

                    _Variables.DongleSN = dongleSN;
                    MainForm.AddStep("Write Machine.Lic Code to " + dongleSN + "'s " + _CONSTANTS.FILE_NAME_DONGLE_INFO);

                    DongleInfoFile dongleFile = DongleInfoFile.Load(dongleSN);
                    dongleFile.xml.BP.Code = bPCode;
                    dongleFile.xml.BP.dateOfReceipt = DateTime.Now;
                    dongleFile.Save();

                    File.Delete(attachmentFileName);
                }
            }
            //If dictionary contains dongle sn
            ////Save attachment to dongle folder
            ////Parse BP Code from saved file and save to dongle info.txt
        }

        public static void UpdateThingworxXML(string name)
        {
            XDocument doc = XDocument.Load(_CONSTANTS.FILE_NAME_THINGWORX_SESSION_VARS,LoadOptions.PreserveWhitespace);
            doc.Root.Element("item").Element("value").Value = name;

            using (XmlWriter writer = XmlWriter.Create(_CONSTANTS.FILE_NAME_THINGWORX_SESSION_VARS))
            {
                
                doc.WriteTo(writer);
                writer.Close();
            }
                
            
            
        }

        #region Network Drive Interaction
        [DllImport("mpr.dll", SetLastError = true, EntryPoint = "WNetRestoreSingleConnectionW", CharSet = CharSet.Unicode)]
        internal static extern int WNetRestoreSingleConnection(IntPtr windowHandle,
                                                            [MarshalAs(UnmanagedType.LPWStr)] string localDrive,
                                                            [MarshalAs(UnmanagedType.Bool)] bool useUI);
        internal static void AttemptDriveReconnect(string drive)
        {
            IntPtr hWnd = new IntPtr(0);
            int res = WNetRestoreSingleConnection(hWnd, drive, false);
        }
        #endregion
    }
}
