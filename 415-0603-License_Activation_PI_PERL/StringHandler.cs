﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    internal static class StringHandler
    {

        /// <summary>
        /// Returns a list of indeces where the VALUE is located in the STR string
        /// </summary>
        /// <param name="str">The string to search within</param>
        /// <param name="value">The string to search for</param>
        /// <returns></returns>
        internal static List<int> AllIndexesOf(string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            List<int> indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
        }

        internal static List<string> FindAllKeyValuesInString(string key, string source, int lengthOfInfo)
        {

            List<string> retList = new List<string>(); ;
            List<int> lockCodeIndeces = StringHandler.AllIndexesOf(source, key);

            foreach (int index in lockCodeIndeces)
            {
                retList.Add(source.Substring(index + key.Length, lengthOfInfo));
            }


            if (retList.Count == 0)
            {
                throw new Exception("Email Scrub error: \r\nReturned List count: " + retList.Count);
            }

            return retList;
        }
    }
}
