﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_Configure_PI_Speed : Task
    {
        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }
        string temp = "";

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Pulling Part# from previous entry");
                    temp = Configuration_App_Instance.PiPN;
                    NextStep();
                    break;
                case 1:
                    DetermineNeedForSpeed(temp);
                    break;
                case 2:
                    //901-0172 ONLY
                    AddStep("Configuring Speed");
                    ThreadHandler.StartNewThread(Thread_Import9010172Xml);
                    break;
                default:
                    Configuration_App_Instance.TRANSPORT_SPEED_CONFIG = TaskStates.PASS;
                    Complete();
                    break;
            }
        }

        void DetermineNeedForSpeed(string partNumber)
        {
            AddStep("Determining need for Speed Change");
            if (partNumber == "901-0171" || partNumber == "801-2103")
            {
                AddStep($"Skipping Speed change for {partNumber}");
                FinalStep();
                return;
            }

            if (partNumber == "901-0172")
            {
                AddStep($"Speed change required for {partNumber}");
                NextStep();
                return;
            }


            Fail(new Exception($"Unrecognized part#: {partNumber}"));
        }

        void Thread_Import9010172Xml(object sender, DoWorkEventArgs args)
        {
            try
            {
                MainForm.SetClipboardText(_CONSTANTS.FILE_NAME_PERL1_CONFIG);
                
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.FileName = $"{_CONSTANTS.FOLDER_PATH_PI_MAIN}\\Pi.Settings.exe";

                bool eChecksPassed = true;
                using (Process p = Process.Start(processInfo))
                {
                    ProcessHandler.SetForegroundWindow(p.MainWindowHandle);

                    Robot_ImportXml();

                    MainForm.StartCountUpProgressBar(30000);
                    Thread.Sleep(30000);

                    MainForm.link.TopMost = false;
                    ClosePrompt(p);
                    MainForm.StopProgressBar();

                    Robot_ExportXml();

                    ClosePrompt(p);
                    MainForm.link.TopMost = true;
                    eChecksPassed &= ECheck_VerifySettings();
                    MainForm.link.TopMost = false;
                    ProcessHandler.SetForegroundWindow(p.MainWindowHandle);
                    MainForm.SendKey("%{F4}");

                    p.WaitForExit();

                    ProcessHandler.SetForegroundWindow(_MainProgram.Handle);
                    MainForm.link.TopMost = true;
                }

                if (eChecksPassed)
                    NextStep();
                else
                    throw new Exception("Exported Settings.xml did not verify correctly");
            }
            catch (Exception ex)
            {
                Fail(ex);
            }
            
        }

        void WaitForPrompt(Process p)
        {
            while (p.Handle == p.MainWindowHandle)
            {
                
                Thread.Sleep(250);
            }

        }

        void ClosePrompt(Process p)
        {
            MainForm.link.TopMost = false;
            //Close prompt that opens
            ProcessHandler.SetForegroundWindow(p.Handle);
            Thread.Sleep(500);
            MainForm.SendKey("{Enter}");
            Thread.Sleep(250);
            //Return to main window
            ProcessHandler.SetForegroundWindow(p.Handle);
        }

        void Robot_ImportXml()
        {
            Thread.Sleep(1000);

            MainForm.SendKey("%{f}");
            MainForm.SendKey("{Down 2}");
            MainForm.SendKey("{Enter}");
            

            Thread.Sleep(1000);

            MainForm.SendKey("^{v}");

            Thread.Sleep(500);

            MainForm.SendKey("{Enter}");

            
        }

        void Robot_ExportXml()
        {
            Thread.Sleep(1000);
            MainForm.SendKey("%{f}");
            Thread.Sleep(250);
            MainForm.SendKey("{Enter}");
            Thread.Sleep(1000);
        }

        bool ECheck_VerifySettings()
        {
            MainForm.StartInfiniteProgressBar();
            Configuration_Pi_Settings xportedXml = new Configuration_Pi_Settings();
            xportedXml.arrayOfconfig = Configuration_Pi_Settings.ArrayOfConfig.Load(_CONSTANTS.FILE_NAME_PI_CONFIG_EXPORT);
            MainForm.StopProgressBar();
            return xportedXml.Check9010172Settings();
        }

        internal override string TaskName
        {
            get
            {
                return "Speed Configure";
            }
        }
    }
}
