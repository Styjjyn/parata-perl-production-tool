﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    internal class Task6_PI_PLC_Update : Task
    {

        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }

        protected override void DoTaskSteps(int taskStep)
        {
            Configuration_App_Instance.PLC_UPDATE_STATE = TaskStates.PENDING;

            switch (taskStep)
            {
                case 0:
                    AddStep("Connect the 'PLC Update' cable to the PC");
                    WaitForPlc();
                    break;
                case 1:
                    AddStep("Opening Latest PLC Update Viewer");
                    OpenPlcUpdate();
                    break;
                default:
                    Configuration_App_Instance.PLC_UPDATE_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }
        }

        private void WaitForPlc()
        {
            StartThreadedMethod(true,
                () => 
                {
                    DeviceHandler.FindPlcLoop();
                    MainForm.StopProgressBar();
                }
                );

        }

        private void OpenPlcUpdate()
        {
            StartThreadedMethod(false,
                () => 
                {
                    ProcessStartInfo info = new ProcessStartInfo();
                    info.FileName = FileHandler.PlcUpdateFilePath;
                    info.UseShellExecute = true;

                    using (Process masterP = Process.Start(info))
                    {
                        masterP.WaitForExit();

                        AddStep("Waiting for Update to fully load");
                        Process[] javaProcs = Process.GetProcessesByName("javaw");
                        Process p = javaProcs[0];
                        p.EnableRaisingEvents = true;
                        p.Exited += GetExitCode;
                        using (p)
                        {
                            MainForm.StartCountUpProgressBar(30000);
                            Thread.Sleep(30000);

                            AddStep("Setting Board to correct entry");
                            
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("%{t}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Down 6}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Right}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Up 2}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            Thread.Sleep(3000);
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Up 2}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Enter}");


                            AddStep("Setting port to correct entry");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("%{t}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Down 7}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Right}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Down 2}");
                            Cursor.Position = new System.Drawing.Point(0, 0);
                            MainForm.SendKey("{Enter}");

                            MainForm.AddStep("Starting Upload");
                            MainForm.SendKey("^{u}");

                            AddStep("Waiting for upload to complete...");
                            MainForm.StartCountUpProgressBar(30000);
                            Thread.Sleep(30000);

                            AddStep("Closing PiPlc.Ino");
                            MainForm.SendKey("%{F4}");
                            p.WaitForExit();
                        }
                    }

                    
                }
                );
        }

        private void GetExitCode(object sender, EventArgs args)
        {
            AddStep("Verifying successful completion of PiPlc.Ino update");
            Process p = sender as Process;
            if (p != null)
            {
                if (p.ExitCode != 0)
                    Fail(new Exception($"PiPlc.ino upload failed. ExitCode: {p.ExitCode}"));
                else
                    NextStep();
            }
        }

        internal override void Fail(Exception e)
        {
            Configuration_App_Instance.PLC_UPDATE_STATE = TaskStates.FAIL;
            base.Fail(e);
            
        }


        internal override string TaskName
        {
            get
            {
                return "PLC Update";
            }
        }
    }
}