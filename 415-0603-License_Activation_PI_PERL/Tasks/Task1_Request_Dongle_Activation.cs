﻿
using System;
using System.Windows.Forms;
using System.ComponentModel;
using static _415_0603_Production_Tool_PI_PERL.FileHandler;
using static _415_0603_Production_Tool_PI_PERL.EMailHandler;
using System.Threading;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task1_Request_Dongle_Activation : Task
    {
        private DongleInfoFile dongleInfoFile;

        internal override bool IsPiEnabled { get { return false; } }

        internal override bool IsOneOffEnabled { get { return true; } }

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    //For 30 seconds, look for the dongle connected to the system
                    MainForm.AddStep("Awaiting Dongle to be connected to the PC");
                    WaitForDongleConnection();
                    break;
                case 1:
                    //Prompt for license type
                    AddStep("User: Select desired license type");
                    if (MainForm.PromptForDongleLicenseType() == DialogResult.Cancel)
                        throw new Exception("User cancelled license selection.");
                    NextStep();
                    break;
                case 2:
                    AddStep("User: Select desired M.O. type");
                    PromptForMoType();
                    NextStep();
                    break;
                case 3:
                    //Read the serial number from the Dongle's device information
                    AddStep("Reading Dongle SN");
                    _Variables.DongleSN = DeviceHandler.GetDongleSerialNumber();
                    NextStep();
                    break;
                case 4:
                    //If no dongle folder found, 
                    AddStep("Searching for Dongle Folder on shared drive");
                    dongleInfoFile = DongleInfoFile.Load(_Variables.DongleSN);
                    NextStep();
                    break;

                case 5:
                    ////generate lock code
                    AddStep("Generating Lock code in 'MIL Config' application");
                    MILConfig.Open(MILConfig.LIC_GEN);
                    NextStep();
                    break;
                case 6:
                    ////store lock code
                    AddStep("Storing Lock Code in dongle folder");
                    _Variables.LockCode = Clipboard.GetText();
                    NextStep();
                    break;
                case 7:
                    Thread_Email();
                    break;
                case 8:
                    AddStep("Storing Dongle SN and Lock Code in 'DongleInfo.txt'");
                    StoreInfoToDongleFile();
                    break;
                default:
                    Complete();
                    break;
            }

        }

        internal void WaitForDongleConnection()
        {
            ThreadHandler.StartNewThread(
                (sender, args) => //Background worker Do Work
                {
                    int timeOut = 30000;
                    int delay = 1000;
                    MainForm.UpdateTimerLabel(timeOut);
                    while (DeviceHandler.IsDongleConnected() == false)
                    {

                        Thread.Sleep(delay);

                        timeOut -= delay;
                        MainForm.UpdateTimerLabel(timeOut);
                        if (timeOut <= 0)
                        {
                            MainForm.FailLastStep("Dongle was not found in time.");
                            BackgroundWorker bgw = sender as BackgroundWorker;
                            bgw.CancelAsync();
                            return;
                        }
                    }

                    MainForm.UpdateTimerLabel(-1);
                },
                (sender, args) =>//Background worker OnWorkComplete
                {
                    if (args.Error == null)
                        NextStep();
                });

        }

        /// <summary>
        /// This function asks the tech what kind of MO is being used for this license: Customer or Internal.
        /// Blister Partner gives us a discount for Internal use only licenses
        /// </summary>
        private void PromptForMoType()
        {
            AddStep("Prompting for MO Type");
            OptionPromptResults results =MainForm.ShowOptionPrompt("Select the M.O. type...", 
                "Is the PI unit that this dongle will be paired with for a customer or internal use?",
                new string[] { "Customer", "Internal" });

            AddStep("Checking dialogresult");
            if (results.dialogResult == DialogResult.Cancel)
                throw new Exception("User cancelled M.O. type selection prompt!");

            AddStep("Setting Veriables.MoType");
            if (results.optionResults[0])
                _Variables.MoType = "Customer";
            else
                _Variables.MoType = "Internal";

            NextStep();
        }

        internal void Thread_Email()
        {
            ThreadHandler.StartNewThread(
                (sender, args)=> {

                    try
                    {
                        ////send email to DVC 
                        AddStep("Preparing email to DVC");
                        CreateDVCEmail();

                        ////Send email to BP
                        AddStep("Preparing email to BP");
                        CreateBPEmail();

                        ////write dongle sn and lock code to folder
                    }
                    catch (Exception ex)
                    {
                        (sender as BackgroundWorker).CancelAsync();
                        Fail(ex);
                    }
                    
                },
                (sender, args)=> {

                    if(args.Error == null)
                        NextStep();
                });

        }

        private void StoreInfoToDongleFile()
        {
            dongleInfoFile.xml.LicenseType = _Variables.LicType;
            dongleInfoFile.xml.MoType = _Variables.MoType;
            dongleInfoFile.xml.DVC.LockCode = _Variables.LockCode;
            dongleInfoFile.xml.DVC.dateOfRequest = DateTime.Now;
            dongleInfoFile.xml.BP.dateOfRequest = DateTime.Now;

            Configuration_App_Global.Settings.cDonglesPendingActivation.Dongle d = new Configuration_App_Global.Settings.cDonglesPendingActivation.Dongle();
            d.DongleId = _Variables.DongleSN;
            d.LockCode = _Variables.LockCode;

            //_MainProgram.emailConfig.settings.DonglesPendingActivation.Add(d);

            dongleInfoFile.Save();
            _MainProgram.emailConfig.settings.Save();

            NextStep();
        }

        internal override string TaskName { get { return "Dongle: Request Activation"; } }


        
    }

    
}
