﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task2_Store_MVC_Dongle_Info : Task
    {
        List<string> lockCodeList;
        List<string> swLicCodeList;

        internal override bool IsPiEnabled { get { return false; } }

        internal override bool IsOneOffEnabled { get { return true; } }

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Opening Outlook");
                    OpenOutlook();
                    break;
                case 1:
                    AddStep("User: Select the email with the Activation Code(s)");
                    WaitForEmailSelection();
                    break;
                case 2:
                    AddStep("Scrubbing for lock codes in email");
                    ScrubEmailForLockCode();
                    break;
                case 3:
                    AddStep("Scrubbing for activation codes in email");
                    ScrubEmailForActivationCode();
                    break;
                case 4:
                    AddStep("Adding activation codes to the dongle's folder");
                    AddActivationCodesToDongleFolder();
                    break;
                default:
                    MainForm.StopProgressBar();
                    Complete();
                    break;
            }

        }

        internal void OpenOutlook()
        {
            MainForm.StartInfiniteProgressBar();
            StartThreadedMethod(false,
               () =>
               {
                   EMailHandler.Event_OnOutlookFilterOpen_Clear();
                   EMailHandler.Event_OnOutlookFilterOpen += new EMailHandler.Delegate_OnOutlookFilterOpen(NextStep);
                   EMailHandler.OpenOutlook("from:machinevision.nl about:software license key about:lock code");
               });

            
        }

        internal void WaitForEmailSelection()
        {
            MainForm.StopProgressBar();
            StartThreadedMethod(true,
                () =>
                {
                    EMailHandler.WaitForUserSelection();

                    while (EMailHandler.SelectedEmail == null)
                    {
                        Thread.Sleep(500);
                    }

                });

        }

        internal void ScrubEmailForLockCode()
        {
            ThreadHandler.StartNewThread(
                (sender, args)=> 
                {
                    try
                    {
                        lockCodeList = StringHandler.FindAllKeyValuesInString("Lock Code: ", EMailHandler.SelectedEmail.Body, _CONSTANTS.LENGTH_LOCK_CODE);
                    }
                    catch (Exception ex)
                    {
                        Fail(ex);
                        throw;
                    }
                    
                },
                (sender, args)=> 
                {
                    if (args.Error == null)
                        NextStep();
                });

            
        }

        internal void ScrubEmailForActivationCode()
        {
            ThreadHandler.StartNewThread(
                (sender, args) =>
                {
                    try
                    {
                        swLicCodeList = StringHandler.FindAllKeyValuesInString("Software License Key: ", EMailHandler.SelectedEmail.Body, _CONSTANTS.LENGTH_ACTIVATION_CODE);
                    }
                    catch (Exception ex)
                    {
                        Fail(ex);
                        throw;
                    }

                },
                (sender, args) =>
                {
                    if (args.Error == null)
                        NextStep();
                });
            
        }

        private void AddActivationCodesToDongleFolder()
        {
            ThreadHandler.StartNewThread(
                (sender, args) =>
                {
                    try
                    {
                        FileHandler.AddActivationCodesFromEmail(lockCodeList, swLicCodeList);
                    }
                    catch (Exception ex)
                    {
                        Fail(ex);
                        throw;
                    }

                },
                (sender, args) =>
                {
                    if (args.Error == null)
                        NextStep();
                });
        }

        internal override void Fail(Exception e)
        {
            base.Fail(e);
        }

        internal override string TaskName
        {
            get
            {
                return "Dongle: Store DVC Info";
            }
        }

    }
}
