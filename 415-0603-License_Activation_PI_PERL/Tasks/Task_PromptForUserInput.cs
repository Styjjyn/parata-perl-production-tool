﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_PromptForUserInput : Task
    {
        internal override bool IsPiEnabled { get { return true; } }

        internal override bool IsOneOffEnabled { get { return false; } }
        
        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    PromptForInput();
                    break;
                default:
                    Configuration_App_Instance.PROMPT_FOR_INPUT_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }
        }

        private void PromptForInput()
        {
            if (MainForm.ShowDialogPrompt(new Prompt_for_Input.GUI_PromptForUserInput()) != System.Windows.Forms.DialogResult.OK)
            {
                throw new Exception("User aborted input prompt.");
            }

            NextStep();
        }

        internal override string TaskName
        {
            get {
                return "Prompt for User Input"; ;
            }
        }

    }
}
