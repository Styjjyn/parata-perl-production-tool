﻿using _415_0603_Production_Tool_PI_PERL.Tasks.Windows_Activation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_Windows_Activation : Task
    {

        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }
        private delegate void Delegate_PromptForKey();

        protected override void DoTaskSteps(int taskStep)
        {
            Configuration_App_Instance.WINDOWS_ACTIVATE_STATE = TaskStates.PENDING;
            switch (taskStep)
            {
                case 0:
                    AddStep("Installing Product Key");
                    ActivateWindows();
                    break;
                case 1:
                    AddStep("Recording Windows 10 License Key in Shared Drive");
                    StoreWindowsKeyInSharedDrive();
                    break;
                default:
                    Configuration_App_Instance.WINDOWS_ACTIVATE_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }

        }

        private delegate string Delegate_GetClipboardText();

        private void ActivateWindows()
        {
            StartThreadedMethod(true,
                () =>
                {
                    string s = Configuration_App_Instance.WindowsKey;
                    Process p;
                    using (p = ProcessHandler.RunElevatedCommandPrompt($"/C slmgr /ipk {s}"))
                    {
                        p.WaitForExit();
                        if (p.ExitCode != 0) throw new Exception($"Activation Key Entry Command Exited with code: {p.ExitCode}");
                    }

                    MainForm.StartCountUpProgressBar(10000);
                    Thread.Sleep(10000);

                    AddStep("Activating Windows Online");
                    using (p = ProcessHandler.RunElevatedCommandPrompt("/C slmgr /ato"))
                    {
                        p.WaitForExit();
                        if (p.ExitCode != 0) throw new Exception($"Online Activation Command Exited with code: {p.ExitCode}");
                    }
                });
        }

        private void StoreWindowsKeyInSharedDrive()
        {

            if (ThreadHandler.IsRunningOnMainThread(StoreWindowsKeyInSharedDrive) == false)
                return;


            FileHandler.AddWinLicFileToPiFolder();
            NextStep();

        }


        internal override void Fail(Exception e)
        {
            Configuration_App_Instance.WINDOWS_ACTIVATE_STATE = TaskStates.FAIL;
            base.Fail(e);

        }

        internal override string TaskName {
            get {
                return "Windows Activation";
            }
        }
    }
}
