﻿using Shell32;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_Cleanup : Task
    {
        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return true; } }

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Validating Perl...");
                    ValidatePerl();
                    break;
                case 1:
                    AddStep("Preparing MFG Files for Salesforce");
                    FileHandler.CreateMfgFolderForSalesforce();
                    NextStep();
                    break;
                case 2:
                    AddStep("Copying MFG File location to clipboard");
                    CopySalesforceLinkToClipboard();
                    break;
                case 3:
                    AddStep("Opening Salesforce");
                    StartThreadedMethod(true, OpenSalesforce);
                    break;
                case 4:

                    Complete();
                    break;

            }
        }

        private void ValidatePerl()
        {
            BitArray successMask = new BitArray(4, true);
            BitArray validations = new BitArray(4);
            BitArray resultant;

            _Variables.PISN = Configuration_App_Instance.PiSn;

            AddStep("Checking for 'FinalFunctionalImage.png'");
            validations[0] = File.Exists($"{_CONSTANTS.FOLDER_PATH_PERL}{_Variables.PISN}\\FinalFunctionalImage.png");

            AddStep("Checking for 'WIN 10 Lic.txt'");
            validations[1] = File.Exists($"{_CONSTANTS.FOLDER_PATH_PERL}{_Variables.PISN}\\WIN 10 Lic.txt");

            AddStep("Checking the Recycle Bin");
            validations[2] = RecycleBinIsClear();

            AddStep("Checking for the 'T:'");
            validations[3] = tDriveIsDisconnected();

            resultant = validations.And(successMask);

            if (resultant.Cast<bool>().Contains(false) == false)
            {
                NextStep();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Errors found:");

                if (validations[0] == false)
                    sb.AppendLine($"Could not locate: {_CONSTANTS.FOLDER_PATH_PERL}{_Variables.PISN}\\FinalFunctionalImage.png");

                if (validations[1] == false)
                    sb.AppendLine($"Could not locate: {_CONSTANTS.FOLDER_PATH_PERL}{_Variables.PISN}\\WIN 10 Lic.txt");

                if (validations[2] == false)
                    sb.AppendLine($"Please empty the Recycle Bin before proceeding");

                if (validations[3] == false)
                    sb.AppendLine($"Please disconnect the 'T:' drive before proceeding");

                Fail(new Exception(sb.ToString()));
            }
        }

        private bool tDriveIsDisconnected()
        {
            
            return Directory.Exists("T:") == false;
        }

        private bool RecycleBinIsClear()
        {
            Shell shell = new Shell();
            Folder recycleBin = shell.NameSpace(10);
            int itemsCount = recycleBin.Items().Count;
            return itemsCount == 0;
        }

        private void CopySalesforceLinkToClipboard()
        {
            //run on main thread
            if (ThreadHandler.IsRunningOnMainThread(CopySalesforceLinkToClipboard) == false)
                return;

            string piSn = Configuration_App_Instance.PiSn;
            string fromPath = $@"{_CONSTANTS.FOLDER_PATH_UNIT_FOLDER}{piSn}";
            string zipPath = $@"{fromPath}\{piSn} MFG Files.zip";
            Clipboard.SetText(zipPath);

            NextStep();
        }

        internal override void Complete()
        {
            base.Complete();
        }

        

        private void OpenSalesforce()
        {
            AddStep("Setting Windows to Auto-start into 'ParataUser' account");
            RegistryHandler.SetParataUserAsDefault();

            AddStep("Opening Salesforce...");
            MainForm.ShowMessageBox(
                "Opening Salesforce",
                    "Salesforce will now open in Google Chrome.\b\r"+
                    "* Refer to the 114-0291 section 6.3 for further instructions");

            ProcessStartInfo pInfo = new ProcessStartInfo();
            pInfo.FileName = "chrome.exe";
            pInfo.Arguments = $@"https://paratasso.my.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=001&sen=068&sen=00T&sen=00U&sen=02i&str=Perl+*+"+$"{Configuration_App_Instance.PiSn}";

            using (Process p = Process.Start(pInfo))
            {
                p.WaitForExit();

            }
        }

        internal override void Fail(Exception e)
        {
            Configuration_App_Instance.FIN_FUN_PREP_STATE = TaskStates.FAIL;
            base.Fail(e);
        }

        internal override string TaskName
        {
            get
            {
                return "114-0291 - Post-Final Functional";
            }
        }
    }
}
