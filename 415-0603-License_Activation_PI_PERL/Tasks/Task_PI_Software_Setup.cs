﻿using System;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{

    /// <summary>
    /// This class will wrap the PC Rename, Dongle Pair, PLC Update and Thingworx installation into 1.
    /// If any of these tasks fail, they will be individually added to the task list.
    /// </summary>
    class Task_PI_Software_Setup : Task
    {
        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return true; } }

        Task4_PI_PC_Rename pcRenameTask;
        Task5_Dongle_Pair_With_PI donglePairTask;
        Task6_PI_PLC_Update plcUpdateTask;
        Task7_PI_ThingWorx_Install thingworxInstallTask;
        Task_Windows_Activation windowsActivationTask;
        Task_FinalFunctionalPrep finalFunctionalTask;
        Task_Configure_PI_Speed configurePiSpeedTask;
        Task_Drive_Check driveCheckTask;
        Task_PromptForUserInput promptForInputTask;

        /// <summary>
        /// This method is the entry point for starting a task
        /// </summary>
        /// <param name="resumeCode">
        /// If the application was restarted in the middle of operation, this is the return point to come back to upon restart
        /// </param>

        #region Task Exceptions
        [Serializable]
        public class Exception_PC_RENAME : Exception
        {
            public Exception_PC_RENAME() { }
            public Exception_PC_RENAME(string message) : base(message) { }
            public Exception_PC_RENAME(string message, Exception inner) : base(message, inner) { }
            protected Exception_PC_RENAME(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        [Serializable]
        public class Exception_PLC_UPDATE : Exception
        {
            public Exception_PLC_UPDATE() { }
            public Exception_PLC_UPDATE(string message) : base(message) { }
            public Exception_PLC_UPDATE(string message, Exception inner) : base(message, inner) { }
            protected Exception_PLC_UPDATE(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        [Serializable]
        public class Exception_THINGWORX_INSTALL : Exception
        {
            public Exception_THINGWORX_INSTALL() { }
            public Exception_THINGWORX_INSTALL(string message) : base(message) { }
            public Exception_THINGWORX_INSTALL(string message, Exception inner) : base(message, inner) { }
            protected Exception_THINGWORX_INSTALL(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        [Serializable]
        public class Exception_DONGLE_PAIR : Exception
        {
            public Exception_DONGLE_PAIR() { }
            public Exception_DONGLE_PAIR(string message) : base(message) { }
            public Exception_DONGLE_PAIR(string message, Exception inner) : base(message, inner) { }
            protected Exception_DONGLE_PAIR(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        [Serializable]
        public class Exception_WINDOWS_ACTIVATE : Exception
        {
            public Exception_WINDOWS_ACTIVATE() { }
            public Exception_WINDOWS_ACTIVATE(string message) : base(message) { }
            public Exception_WINDOWS_ACTIVATE(string message, Exception inner) : base(message, inner) { }
            protected Exception_WINDOWS_ACTIVATE(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        #endregion


        bool firstRun = false;
        protected override void DoTaskSteps(int taskStep)
        {
            if (firstRun == false)
            {
                MainForm.ResetStepdisplay();
                firstRun = true;
            }

            switch (taskStep)
            {
                case 0:
                    RunTask(promptForInputTask, "Prompting for user input", 0, Configuration_App_Instance.PROMPT_FOR_INPUT_STATE);
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    RunTask(pcRenameTask, "Renaming PC", taskStep-1, Configuration_App_Instance.PC_RENAME_STATE);
                    break;
                case 8:
                    RunTask(donglePairTask, "Pairing Dongle to PI", 0, Configuration_App_Instance.DONGLE_PAIR_STATE);
                    break;
                case 9:
                    RunTask(thingworxInstallTask, "Installing Thingworx", 0, Configuration_App_Instance.THINGWORX_STATE);
                    break;
                case 10:
                    RunTask(plcUpdateTask, "Updating PLC Software", 0, Configuration_App_Instance.PLC_UPDATE_STATE);
                    break;
                case 11:
                    RunTask(windowsActivationTask, "Activating Windows", 0, Configuration_App_Instance.WINDOWS_ACTIVATE_STATE);
                    break;
                case 12:
                    RunTask(configurePiSpeedTask, "Configuring Transport Speed", 0, Configuration_App_Instance.TRANSPORT_SPEED_CONFIG);
                    break;
                case 13:
                    RunTask(driveCheckTask, "Checking For D: and P: Drives", 0, Configuration_App_Instance.DRIVE_CHECK_TASK);
                    break;
                case 14:
                    RunTask(finalFunctionalTask, "Prepping for Final Functional", 0, Configuration_App_Instance.FIN_FUN_PREP_STATE);
                    break;
                default:
                    Complete();
                    break;
            }
        }

        internal void RunTask(Task taskToRun, string stepText, int startingIndex, TaskStates relatedTaskState)
        {
            if (relatedTaskState != TaskStates.PASS)
            {
                taskToRun.Event_OnComplete_Clear();
                taskToRun.Event_OnComplete += new Delegate_OnComplete(() => { NextStep(); });
                taskToRun.Do(startingIndex);
            }
            else
            {
                AddStep(stepText);
                Complete();
                NextStep();
            }
        }

        internal override void Fail(Exception e)
        {
            base.Fail(e);
        }

        internal override string TaskName {
            get {
                return "114-0293 - Software Setup";
            }
        }

        internal override void Initialize()
        {
            promptForInputTask = new Task_PromptForUserInput();
            pcRenameTask = new Task4_PI_PC_Rename();
            donglePairTask = new Task5_Dongle_Pair_With_PI();
            plcUpdateTask = new Task6_PI_PLC_Update();
            thingworxInstallTask = new Task7_PI_ThingWorx_Install();
            windowsActivationTask = new Task_Windows_Activation();
            finalFunctionalTask = new Task_FinalFunctionalPrep();
            configurePiSpeedTask = new Task_Configure_PI_Speed();
            driveCheckTask = new Task_Drive_Check();
        }
    }
}
