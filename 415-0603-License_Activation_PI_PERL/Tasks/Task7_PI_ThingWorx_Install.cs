﻿using _415_0603_Production_Tool_PI_PERL;
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    internal class Task7_PI_ThingWorx_Install : Task
    {

        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }

        Process[] processesRunning;
        Process easyButtonProcess;
        Process imProcess;

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Launching Thingworx IM");
                    LaunchThingworxIM();
                    break;
                case 1:
                    AddStep("Please follow the instructions in the Easy Button Process...");
                    WaitForIMCompletion();
                    break;
                case 2:
                    AddStep("Verifying Thingworx Installation");
                    CheckForThingworxProcesses();
                    break;
                default:
                    Configuration_App_Instance.THINGWORX_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }
        }

        private void LaunchThingworxIM()
        {
            StartThreadedMethod(true,
                ()=> 
                {

                    string thingworkExePath = FileHandler.ThingworxInstallerExe;
                    ProcessStartInfo pInfo = new ProcessStartInfo(thingworkExePath);
                    pInfo.UseShellExecute = false;
                    pInfo.RedirectStandardOutput = true;
                    Process proc = Process.Start(pInfo);
                    proc.WaitForExit();

                    AddStep("Locating Easy Button process");

                    Process[] runningProcs = Process.GetProcessesByName("EasyButton");

                    using (easyButtonProcess = runningProcs[0])
                    {
                        easyButtonProcess.WaitForExit();
                    }
                });

            
        }

        private void WaitForIMCompletion()
        {
            StartThreadedMethod(true,
                () =>
                {
                    Thread.Sleep(1000);

                    Process[] runningProcs = Process.GetProcessesByName("InstallManager");

                    using (Process p = runningProcs[0])
                    {
                        p.WaitForExit();
                    }

                    
                });
        }

        private void CheckForThingworxProcesses()
        {
            StartThreadedMethod(true,
                ()=> 
                {
                    bool pass = true;

                    MainForm.StartCountUpProgressBar(30000);
                    Thread.Sleep(30000);
                 

                    //Check for thingworx processes running after installer completes.
                    ServiceController twService1 = new ServiceController("Thingworx_LSR");
                    ServiceController twService2 = new ServiceController("Thingworx_WSEMS");
                    ServiceController twService3 = new ServiceController("Parata Thingworx Agent");

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Thingworx was not properlly installed.");

                    if ((twService1.Status == ServiceControllerStatus.Stopped) == false)
                    {
                        pass &= false;
                        sb.AppendLine("Thingworx_LSR process not found.");
                    }

                    if ((twService2.Status == ServiceControllerStatus.Stopped) == false)
                    {
                        pass &= false;
                        sb.AppendLine("Thingworx_WSEMS process not found.");
                    }

                    if ((twService3.Status == ServiceControllerStatus.Running) == false)
                    {
                        pass &= false;
                        sb.AppendLine("ParataThingworxAgent process not found.");
                    }

                    if (!pass)
                    {
                        sb.AppendLine("Please re-run this task!!!");

                    throw new Exception(sb.ToString());
                    }
                });
        }

        void GetAllRunningProcesses()
        {
            processesRunning = Process.GetProcesses();
            
        }

        Process FindEasyButtonProcess()
        {
            return easyButtonProcess = FindProcessByName("EasyButton");

        }

        Process FindProcessByName(string name)
        {
            Process ret = null;
            if (processesRunning == null)
                GetAllRunningProcesses();
            foreach (Process p in processesRunning)
            {
                if (p.ProcessName == name)
                {
                    ret = p;
                    break;
                }

            }

            return ret;
        }

        Process FindProcessContainingName(string name)
        {
            Process ret = null;
            if (processesRunning == null)
                GetAllRunningProcesses();
            foreach (Process p in processesRunning)
            {
                MainForm.ShowYesNoMessageBox("",p.ProcessName);
                if (p.ProcessName.Contains(name))
                {
                    ret = p;
                    break;
                }

            }

            return ret;
        }

        internal override void Fail(Exception ex)
        {
            base.Fail(ex);
            Configuration_App_Instance.THINGWORX_STATE = TaskStates.FAIL;
        }


        internal override string TaskName
        {
            get
            {
                return "Thingworx Install";
            }
        }
    }
}