﻿
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_PLC_Differentiate : Task
    {
        internal override bool IsPiEnabled { get { return true; } }

        internal override bool IsOneOffEnabled { get { return true; } }
        private SerialPort serialPort;

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    OpenSerialPort();
                    break;
                case 1:
                    SendSerialDataRequest();
                    break;
                default:
                    break;
            }
        }

        private void OpenSerialPort()
        {
            AddStep("Opening Serial Port connection to PLC...");
            serialPort = new SerialPort();
            serialPort.DataReceived += SerialPort_DataReceived;
            serialPort.PortName = "COM7";
            serialPort.Open();
            NextStep();
        }

        bool plcIded = false;
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string s = serialPort.ReadExisting();
            serialPort.Close();
            if (s.Contains("PI"))
            {
                AddStep("PLC Identified as Pouch Inspector");
                MessageBox.Show("PASS");
                plcIded = true;
                NextStep();
                return;
            }

        }

        private void SendSerialDataRequest()
        {
            StartThreadedMethod(false, ()=>{ WaitFor(3000); });

            AddStep("Requesting PLC to identify self");
            serialPort.WriteLine("ID");
        }

        private void WaitFor(int millis)
        {
            MainForm.StartCountUpProgressBar(millis);
            while (millis >= 0 && plcIded == false)
            {
                Thread.Sleep(100);
                millis -= 100;
            }
            MainForm.StopProgressBar();
            serialPort.Close();

            if (plcIded) return;

            AddStep("PLC Identified as Cut and Roll");
            MessageBox.Show("FAIL");
            NextStep();
        }

        internal override string TaskName
        {
            get
            {
                return "TEST - PLC Comm";
            }
        }
    }
}
