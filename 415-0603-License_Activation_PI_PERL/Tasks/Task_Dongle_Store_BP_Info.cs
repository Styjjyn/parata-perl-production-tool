﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_Dongle_Store_BP_Info : Task
    {
        Dictionary<string, Outlook.Attachment> info;

        internal override bool IsPiEnabled { get { return false; } }
        internal override bool IsOneOffEnabled { get { return true; } }

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Opening Outlook");
                    OpenOutlook();
                    break;
                case 1:
                    AddStep("User: Select the email with the Activation Code(s)");
                    WaitForEmailSelect();
                    break;
                case 2:
                    AddStep("Scrubbing for Machine.lic files in email");
                    ScrubAttachmentsForMachLics();
                    break;
                default:
                    Complete();
                    break;
            }
        }

        private void OpenOutlook()
        {
            StartThreadedMethod(false, 
                ()=> 
                {
                    EMailHandler.Event_OnOutlookFilterOpen_Clear();
                    EMailHandler.Event_OnOutlookFilterOpen += new EMailHandler.Delegate_OnOutlookFilterOpen(NextStep);
                    EMailHandler.OpenOutlook("attachments: machine.lic");
                });

        }


        private void WaitForEmailSelect()
        {

            StartThreadedMethod(true,
                () =>
                {
                    EMailHandler.WaitForUserSelection();

                    while (EMailHandler.SelectedEmail == null)
                    {
                        Thread.Sleep(500);
                    }

                });
            
        }

        private void ScrubAttachmentsForMachLics()
        {
            StartThreadedMethod(true,
                ()=>
                {
                    info = EMailHandler.ScrubAttachmentsForMachLicNumber();

                    FileHandler.AddMachineLicCodesFromEmail(info);
                });
        }

        internal override void Fail(Exception e)
        {
            base.Fail(e);
        }

        internal override string TaskName
        {
            get
            {
                return "Dongle: Store BP Info";
            }
        }
    }
}
