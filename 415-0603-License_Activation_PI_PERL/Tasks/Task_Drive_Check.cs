﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_Drive_Check : Task
    {
        internal override bool IsPiEnabled { get { return true; } }

        internal override bool IsOneOffEnabled { get { return false; } }

        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Verifying D: exists");
                    CheckForDrive(@"D:\", new Exception("D: Drive not found!!! Please ensure that the drive was created correctly."));
                    break;

                case 1:
                    AddStep("Verifying P: exists");
                    StringBuilder sb = new StringBuilder("P: Drive not found!!!");
                    sb.AppendLine("Please ensure the following:");
                    sb.AppendLine(" * The drive was created correctly.");
                    sb.AppendLine(" * The RAID Enclosure is connected to the PC");
                    sb.AppendLine(" * The RAID Enclosure is powered on.");
                    CheckForDrive(@"P:\", new Exception(sb.ToString()));
                    break;
                default:
                    Complete();
                    break;
            }
        }

        /// <summary>
        /// Checks to see if the drive name exists. Drive name must be in the format 'X:\'
        /// </summary>
        /// <param name="driveLetter"></param>
        private void CheckForDrive(string driveLetter, Exception throwOnFail)
        {

            DriveInfo drive = DriveInfo.GetDrives().Where((d) => d.Name == driveLetter).FirstOrDefault();

            if (drive == null)
                throw throwOnFail;
            else
                NextStep();
        }

        internal override string TaskName { get { return "D:/P: Check"; } }

    }
}
