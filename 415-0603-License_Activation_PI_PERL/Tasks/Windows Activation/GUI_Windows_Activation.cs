﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks.Windows_Activation
{
    public partial class GUI_Windows_Activation : Form
    {
        public GUI_Windows_Activation()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           textBox1.Text = textBox1.Text.Replace("-","");
           button1.Enabled = textBox1.Text.Length == 25;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DialogResult = DialogResult.Yes;
            Close();
        }

        internal string LicenseKey { get {

                MainForm.AddStep("Fetching formatted License Key");
                string formatted = "";
                string old = textBox1.Text;
                formatted = $"{old.Substring(0,5)}-{old.Substring(5,5)}-{old.Substring(10,5)}-{old.Substring(15,5)}-{old.Substring(20,5)}";
                MessageBox.Show($"LicenseKey: {formatted}");
                return formatted; } }
    }
}
