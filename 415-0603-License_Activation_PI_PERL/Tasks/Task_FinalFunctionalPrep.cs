﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class Task_FinalFunctionalPrep : Task
    {
        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }


        protected override void DoTaskSteps(int taskStep)
        {
            Configuration_App_Instance.FIN_FUN_PREP_STATE = TaskStates.PENDING;
            switch (taskStep)
            {
                case 0:
                    StringBuilder sb = new StringBuilder();
                    sb.Append("copying all Batch.out files from:");
                    sb.Append($"{_CONSTANTS.FOLDER_PATH_BATCH_MASTER}");
                    AddStep(sb.ToString());
                    Thread_FindAllBatchFiles();
                    break;
                default:
                    Configuration_App_Instance.FIN_FUN_PREP_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }
        }

        private void Thread_FindAllBatchFiles()
        {
            ThreadHandler.StartNewThread(
                (sender, args)=> {
                    FindAllBatchFiles();
                },
                (sender, args) => {
                    //On complete
                    NextStep();
                });
        }

        private void FindAllBatchFiles()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(_CONSTANTS.FOLDER_PATH_TEST_BATCH);
            foreach (FileInfo file in dirInfo.GetFiles("*.OUT"))
            {
                file.Delete();
            }

            dirInfo = new DirectoryInfo(_CONSTANTS.FOLDER_PATH_BATCH_MASTER);
            FileInfo[] filesInDir = dirInfo.GetFiles("*.OUT");

            foreach (FileInfo file in filesInDir)
            {
                ProcessStartInfo pInfo = new ProcessStartInfo();
                pInfo.FileName = "cmd.exe";
                pInfo.Arguments = ($"/C echo F|xcopy \"{file.FullName}\" \"{_CONSTANTS.FOLDER_PATH_TEST_BATCH}{file.Name}\"");
                pInfo.CreateNoWindow = true;
                using (Process p = Process.Start(pInfo))
                {
                    p.WaitForExit();
                    if (p.ExitCode != 0)
                    {
                        Fail(new Exception($"Failed copying files with exitCode: {p.ExitCode}"));
                        return;
                    } 
                }
            }
        }

        internal override string TaskName {
            get {
                return "Final Functional Prep";
            }
        }

    }
}
