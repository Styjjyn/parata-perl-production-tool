﻿namespace _415_0603_Production_Tool_PI_PERL.Tasks._1_Dongle_Request_Activation
{
    partial class GUI_Prompt_Dongle_LicenseType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radButton_PI_ONLY = new System.Windows.Forms.RadioButton();
            this.radButton_PI_CR = new System.Windows.Forms.RadioButton();
            this.button_Confirm = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button_Confirm, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button_Cancel, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(486, 129);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textBox1, 2);
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(480, 31);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Please the select the type of license this Dongle will need:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.radButton_PI_CR, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.radButton_PI_ONLY, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 40);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(480, 34);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radButton_PI_ONLY
            // 
            this.radButton_PI_ONLY.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radButton_PI_ONLY.AutoSize = true;
            this.radButton_PI_ONLY.Location = new System.Drawing.Point(78, 5);
            this.radButton_PI_ONLY.Name = "radButton_PI_ONLY";
            this.radButton_PI_ONLY.Size = new System.Drawing.Size(84, 24);
            this.radButton_PI_ONLY.TabIndex = 0;
            this.radButton_PI_ONLY.TabStop = true;
            this.radButton_PI_ONLY.Text = "PI Only";
            this.radButton_PI_ONLY.UseVisualStyleBackColor = true;
            this.radButton_PI_ONLY.CheckedChanged += new System.EventHandler(this.radButton_PI_ONLY_CheckedChanged);
            // 
            // radButton_PI_CR
            // 
            this.radButton_PI_CR.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radButton_PI_CR.AutoSize = true;
            this.radButton_PI_CR.Location = new System.Drawing.Point(314, 5);
            this.radButton_PI_CR.Name = "radButton_PI_CR";
            this.radButton_PI_CR.Size = new System.Drawing.Size(92, 24);
            this.radButton_PI_CR.TabIndex = 1;
            this.radButton_PI_CR.TabStop = true;
            this.radButton_PI_CR.Text = "PI+C&&R";
            this.radButton_PI_CR.UseVisualStyleBackColor = true;
            this.radButton_PI_CR.CheckedChanged += new System.EventHandler(this.radButton_PI_CR_CheckedChanged);
            // 
            // button_Confirm
            // 
            this.button_Confirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_Confirm.Enabled = false;
            this.button_Confirm.Location = new System.Drawing.Point(3, 80);
            this.button_Confirm.Name = "button_Confirm";
            this.button_Confirm.Size = new System.Drawing.Size(156, 45);
            this.button_Confirm.TabIndex = 2;
            this.button_Confirm.Text = "CONFIRM";
            this.button_Confirm.UseVisualStyleBackColor = false;
            this.button_Confirm.Click += new System.EventHandler(this.button_Confirm_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button_Cancel.Location = new System.Drawing.Point(327, 80);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(156, 42);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "CANCEL";
            this.button_Cancel.UseVisualStyleBackColor = false;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // GUI_Prompt_Dongle_LicenseType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 154);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "GUI_Prompt_Dongle_LicenseType";
            this.Text = "GUI_Prompt_Dongle_LicenseType";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton radButton_PI_CR;
        private System.Windows.Forms.RadioButton radButton_PI_ONLY;
        private System.Windows.Forms.Button button_Confirm;
        private System.Windows.Forms.Button button_Cancel;
    }
}