﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks._1_Dongle_Request_Activation
{
    public partial class GUI_Prompt_Dongle_LicenseType : Form
    {
        public GUI_Prompt_Dongle_LicenseType()
        {
            InitializeComponent();
        }

        private void button_Confirm_Click(object sender, EventArgs e)
        {
            string l = radButton_PI_ONLY.Checked == true ? "PI ONLY" : "PI+C&R";
            _Variables.LicType = l;
            DialogResult = DialogResult.OK;
            Close();

        }

        private void radButton_PI_ONLY_CheckedChanged(object sender, EventArgs e)
        {
            button_Confirm.Enabled = true;
        }

        private void radButton_PI_CR_CheckedChanged(object sender, EventArgs e)
        {
            button_Confirm.Enabled = true;
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
