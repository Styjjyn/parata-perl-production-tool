﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    class ECheckResult
    {
        public List<ECheck> echecks = new List<ECheck>();
        

    }

    class ECheck
    {
        public string DescriptionOfCheck;
        public bool BooleanCheck;
        public Exception ThrownOnFail;
    }
}
