﻿namespace _415_0603_Production_Tool_PI_PERL.Tasks._5_PC_Rename
{
    partial class scanEntry_PC_Rename
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_PerlSN = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scan the Barcode on the rear of the PC:";
            // 
            // textBox_PerlSN
            // 
            this.textBox_PerlSN.Location = new System.Drawing.Point(294, 4);
            this.textBox_PerlSN.Name = "textBox_PerlSN";
            this.textBox_PerlSN.Size = new System.Drawing.Size(405, 26);
            this.textBox_PerlSN.TabIndex = 1;
            this.textBox_PerlSN.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // scanEntry_PC_Rename
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox_PerlSN);
            this.Controls.Add(this.label1);
            this.Name = "scanEntry_PC_Rename";
            this.Size = new System.Drawing.Size(702, 34);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox textBox_PerlSN;
    }
}
