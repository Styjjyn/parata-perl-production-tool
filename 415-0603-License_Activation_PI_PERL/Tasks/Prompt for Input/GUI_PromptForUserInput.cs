﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _415_0603_Production_Tool_PI_PERL.Tasks.Prompt_for_Input
{
    public partial class GUI_PromptForUserInput : Form
    {
        public UserInput userInput;

        public GUI_PromptForUserInput()
        {
            InitializeComponent();
            LoadDefaults();
        }

        private void LoadDefaults()
        {
            textBox_PartNumber.Text = Configuration_App_Instance.PiPN;
            textBox_SerialNumber.Text = Configuration_App_Instance.PiSn;
            textBox_WindowsKey.Text = Configuration_App_Instance.WindowsKey;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Configuration_App_Instance.PiPN = textBox_PartNumber.Text;
            Configuration_App_Instance.PiSn = textBox_SerialNumber.Text;
            Configuration_App_Instance.WindowsKey = textBox_WindowsKey.Text;

            DialogResult = DialogResult.OK;

            Close();
        }

        private void GUI_PromptForUserInput_Load(object sender, EventArgs e)
        {

        }

        private void cb_SerialNumber_CheckedChanged(object sender, EventArgs e)
        {
            textBox_PartNumber.Enabled = cb_SerialNumber.Checked;
            EnableProceedButton();
            
        }

        private void cb_PartNumber_CheckedChanged(object sender, EventArgs e)
        {
            textBox_WindowsKey.Enabled = cb_PartNumber.Checked;
            EnableProceedButton();
        }

        private void cb_ProductKey_CheckedChanged(object sender, EventArgs e)
        {
            EnableProceedButton();
        }

        private void EnableProceedButton()
        {
            button1.Enabled = cb_SerialNumber.Checked
                            && cb_ProductKey.Checked
                            && cb_PartNumber.Checked;
        }

        private void textBox_SerialNumber_TextChanged(object sender, EventArgs e)
        {

            cb_SerialNumber.Enabled = (textBox_SerialNumber.Text.Length == 13);
        }

        private void textBox_PartNumber_TextChanged(object sender, EventArgs e)
        {
            cb_PartNumber.Enabled = (textBox_PartNumber.Text.Length == 8);
        }

        private void textBox_WindowsKey_TextChanged(object sender, EventArgs e)
        {
            cb_ProductKey.Enabled = (textBox_WindowsKey.Text.Length == 29);
        }

        public struct UserInput
        {
            public string SerialNumber;
            public string PartNumber;
            public string WindowsKey;
        }


    }
}
