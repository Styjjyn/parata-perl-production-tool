﻿namespace _415_0603_Production_Tool_PI_PERL.Tasks.Prompt_for_Input
{
    partial class GUI_PromptForUserInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cb_ProductKey = new System.Windows.Forms.CheckBox();
            this.cb_PartNumber = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_SerialNumber = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox_WindowsKey = new System.Windows.Forms.MaskedTextBox();
            this.textBox_SerialNumber = new System.Windows.Forms.MaskedTextBox();
            this.textBox_PartNumber = new System.Windows.Forms.MaskedTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.cb_ProductKey, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb_PartNumber, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb_SerialNumber, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBox_WindowsKey, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox_SerialNumber, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_PartNumber, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(644, 132);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cb_ProductKey
            // 
            this.cb_ProductKey.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cb_ProductKey.AutoSize = true;
            this.cb_ProductKey.Enabled = false;
            this.cb_ProductKey.Location = new System.Drawing.Point(552, 68);
            this.cb_ProductKey.Name = "cb_ProductKey";
            this.cb_ProductKey.Size = new System.Drawing.Size(89, 24);
            this.cb_ProductKey.TabIndex = 8;
            this.cb_ProductKey.Text = "Verified";
            this.cb_ProductKey.UseVisualStyleBackColor = true;
            this.cb_ProductKey.CheckedChanged += new System.EventHandler(this.cb_ProductKey_CheckedChanged);
            // 
            // cb_PartNumber
            // 
            this.cb_PartNumber.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cb_PartNumber.AutoSize = true;
            this.cb_PartNumber.Enabled = false;
            this.cb_PartNumber.Location = new System.Drawing.Point(552, 36);
            this.cb_PartNumber.Name = "cb_PartNumber";
            this.cb_PartNumber.Size = new System.Drawing.Size(89, 24);
            this.cb_PartNumber.TabIndex = 7;
            this.cb_PartNumber.Text = "Verified";
            this.cb_PartNumber.UseVisualStyleBackColor = true;
            this.cb_PartNumber.CheckedChanged += new System.EventHandler(this.cb_PartNumber_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pouch Inspector Serial#:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pouch Inspector Part#:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Windows Product Key:";
            // 
            // cb_SerialNumber
            // 
            this.cb_SerialNumber.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cb_SerialNumber.AutoSize = true;
            this.cb_SerialNumber.Enabled = false;
            this.cb_SerialNumber.Location = new System.Drawing.Point(552, 4);
            this.cb_SerialNumber.Name = "cb_SerialNumber";
            this.cb_SerialNumber.Size = new System.Drawing.Size(89, 24);
            this.cb_SerialNumber.TabIndex = 6;
            this.cb_SerialNumber.Text = "Verified";
            this.cb_SerialNumber.UseVisualStyleBackColor = true;
            this.cb_SerialNumber.CheckedChanged += new System.EventHandler(this.cb_SerialNumber_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button1.AutoSize = true;
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.BackColor = System.Drawing.Color.Lime;
            this.tableLayoutPanel1.SetColumnSpan(this.button1, 2);
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(563, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 30);
            this.button1.TabIndex = 9;
            this.button1.Text = "Proceed";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox_WindowsKey
            // 
            this.textBox_WindowsKey.Location = new System.Drawing.Point(191, 67);
            this.textBox_WindowsKey.Mask = "AAAAA-AAAAA-AAAAA-AAAAA-AAAAA";
            this.textBox_WindowsKey.Name = "textBox_WindowsKey";
            this.textBox_WindowsKey.Size = new System.Drawing.Size(355, 26);
            this.textBox_WindowsKey.TabIndex = 10;
            this.textBox_WindowsKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox_WindowsKey.TextChanged += new System.EventHandler(this.textBox_WindowsKey_TextChanged);
            // 
            // textBox_SerialNumber
            // 
            this.textBox_SerialNumber.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.textBox_SerialNumber.Location = new System.Drawing.Point(191, 3);
            this.textBox_SerialNumber.Mask = "AAAAAAA-AAAAA";
            this.textBox_SerialNumber.Name = "textBox_SerialNumber";
            this.textBox_SerialNumber.Size = new System.Drawing.Size(355, 26);
            this.textBox_SerialNumber.TabIndex = 11;
            this.textBox_SerialNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox_SerialNumber.TextChanged += new System.EventHandler(this.textBox_SerialNumber_TextChanged);
            // 
            // textBox_PartNumber
            // 
            this.textBox_PartNumber.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.textBox_PartNumber.Location = new System.Drawing.Point(191, 35);
            this.textBox_PartNumber.Mask = "AAA-AAAA";
            this.textBox_PartNumber.Name = "textBox_PartNumber";
            this.textBox_PartNumber.Size = new System.Drawing.Size(355, 26);
            this.textBox_PartNumber.TabIndex = 12;
            this.textBox_PartNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox_PartNumber.TextChanged += new System.EventHandler(this.textBox_PartNumber_TextChanged);
            // 
            // GUI_PromptForUserInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(736, 135);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GUI_PromptForUserInput";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Please Scan/Enter the requested information";
            this.Load += new System.EventHandler(this.GUI_PromptForUserInput_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox cb_ProductKey;
        private System.Windows.Forms.CheckBox cb_PartNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb_SerialNumber;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox textBox_WindowsKey;
        private System.Windows.Forms.MaskedTextBox textBox_SerialNumber;
        private System.Windows.Forms.MaskedTextBox textBox_PartNumber;
    }
}