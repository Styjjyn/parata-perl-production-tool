﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using static _415_0603_Production_Tool_PI_PERL.FileHandler;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    internal class Task5_Dongle_Pair_With_PI : Task
    {

        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }

        DongleInfoFile dongleInfoFile;

        protected override void DoTaskSteps(int taskStep)
        {
            Configuration_App_Instance.DONGLE_PAIR_STATE = TaskStates.PENDING;

            if (_Variables.PISN == null)
            {
                _Variables.PISN = Configuration_App_Instance.PiSn;
            }

            switch (taskStep)
            {
                case 0:
                    AddStep("waiting for Dongle to be connected");
                    WaitForDongle();
                    break;
                case 1:
                    AddStep("Retrieving Dongle SN from device");
                    _Variables.DongleSN = DeviceHandler.GetDongleSerialNumber();
                    NextStep();
                    break;
                case 2:
                    AddStep("Checking Dongle info file for exisitng Pouch Inspector Pairing");
                    CheckForPreviousDonglePairing();
                    
                    break;
                case 3:
                    AddStep("Checking Dongle info file for existing Machine License code");
                    CheckForMachineLicFile();
                    break;
                case 4:
                    AddStep("Checking Dongle info file for existing DVC Activation code");
                    CheckForDvcCode();
                    break;
                case 5:
                    AddStep("Activating Dongle through Mil Config application");
                    MILConfig.Open(MILConfig.LIC_ACT, NextStep);
                    break;
                case 6:
                    AddStep("Creating Machine License File based on Dongle");
                    FileHandler.CreateMachineLicenseFile();
                    NextStep();
                    break;
                case 7:
                    AddStep("Recording PI SN in Dongle info file");
                    RecordPiInfoToDongleFile();
                    break;
                case 8:
                    AddStep("Copying Dongle folder to PI folder");
                    CopyDongleFolderToPiFolder();
                    break;
                case 9:
                    AddStep("Checking License Type");
                    CheckLicenseType();
                    break;
                case 10:
                    AddStep("Verifying Dongle Pairing");
                    VerifyPairing();
                    break;
                default:
                    Configuration_App_Instance.DONGLE_PAIR_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }
           
 
        }

        private void WaitForDongle()
        {
            StartThreadedMethod(true,
                () =>
                {
                    try
                    {
                        //wait for dongle connection or timeout
                        DeviceHandler.FindDongleLoop();
                    }
                    catch (Exception e)
                    {
                        Fail(e);
                        throw;
                    }
                });
          
        }

        private void CheckForPreviousDonglePairing()
        {
            //check Dongle file.txt for existing PI SN pairing
            dongleInfoFile = DongleInfoFile.Load(_Variables.DongleSN);
            string piPair = dongleInfoFile.PairedPi;
            if (piPair.Length > 0 && MainForm.ShowYesNoMessageBox("Overwrite Paired PI SN?", "This dongle appears to already be paired with the PI SN: " + piPair + ".\r\nWould you like to overwrite this SN with: " + _Variables.PISN) == DialogResult.No)
                throw new Exception("PI Pairing exists. User declined overwrite.");
            else
                dongleInfoFile.PairedPi = _Variables.PISN;

            NextStep();
        }

        private void CheckForMachineLicFile()
        {
            //check dongle file for machine.lic code
            _Variables.BpCode = dongleInfoFile.BpCode;
            if (_Variables.BpCode == null) throw new Exception("The dongle info file for Dongle-" + _Variables.DongleSN + " does NOT contain a Blister Partner code.");

            NextStep();
        }

        private void CheckForDvcCode()
        {
            if (ThreadHandler.IsRunningOnMainThread(CheckForDvcCode) == false)
                return;

            //check dongle file for activation code
            ////Inform user if not found
            _Variables.DvcCode = dongleInfoFile.DvcCode;
            AddStep($"Copying LockCode to Clipboard. LockCode: {_Variables.DvcCode}");
            MainForm.StartInfiniteProgressBar();
            StartThreadedMethod(true,
                () =>
                {
                    Thread.Sleep(1000);
                    for (int i = 100; i < 5000; i += 100)
                    {
                        try
                        {
                            MainForm.SetClipboardText(_Variables.DvcCode);

                            break;
                        }
                        catch (ExternalException)
                        {
                            Thread.Sleep(100);
                        }
                    }
                    MainForm.StopProgressBar();
                    if (_Variables.DvcCode == null) throw new Exception("The dongle info file for Dongle-" + _Variables.DongleSN + " does NOT contain a DVC activation code.");
                });

        }

        private void RecordPiInfoToDongleFile()
        {

            dongleInfoFile.PairedPi = _Variables.PISN;
            dongleInfoFile.DatePaired = DateTime.Now;

            NextStep();
        }

        private void CopyDongleFolderToPiFolder()
        {
            StartThreadedMethod(true,
                ()=>
                {
                    try
                    {
                        FileHandler.CopyDongleFolderToPiFolder();
                    }
                    catch (Exception ex)
                    {
                        Fail(ex);
                        throw;
                    }
                });
        }

        private void CheckLicenseType()
        {
            
            //check dongle file for license type
            if (dongleInfoFile.LicenseType == _CONSTANTS.LICENSE_PI_CR)
            {
                AddStep("Setting up Pouch Inspector for Cut and Roll usage");
                ////If C&R 
                ////Run Pi.Settings
                ////Change 'usecutandroll' to false
                ////save and close
                ProcessHandler.RunPiSettingsFix();
            }

            NextStep();
        }

        private void VerifyPairing()
        {
            NextStep();
        }

        internal override void Fail(Exception e)
        {
            Configuration_App_Instance.DONGLE_PAIR_STATE = TaskStates.FAIL;
            base.Fail(e);
            
        }

        internal override string TaskName
        {
            get
            {
                return "Dongle: Pair with PI";
            }
        }
    }
}