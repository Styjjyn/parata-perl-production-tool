﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Win32.SafeHandles;
using System.Security.Principal;

namespace _415_0603_Production_Tool_PI_PERL.Tasks
{
    internal class Task4_PI_PC_Rename : Task
    {

        internal override bool IsPiEnabled { get { return true; } }
        internal override bool IsOneOffEnabled { get { return false; } }
        string newName;

        enum ResumeCodes
        {
            POST_RENAME_RESUME = 7
        }


        protected override void DoTaskSteps(int taskStep)
        {
            switch (taskStep)
            {
                case 0:
                    AddStep("Pulling Serial Number from previous entry");
                    _Variables.PISN = Configuration_App_Instance.PiSn;
                    NextStep();
                    break;
                case 1:
                    AddStep("Create PI Folder on shared drive");
                    newName = _Variables.PISN.Replace("-", "") + "PER";
                    FileHandler.CreatePIFolder();
                    NextStep();
                    break;
                case 2:
                    //Change 5 registry entries to the new PC name
                    AddStep("Renaming PC");
                    RegistryHandler.PC_Name = newName;
                    NextStep();
                    break;
                case 3:
                    //Left this step to keep continuity 
                    NextStep();
                    break;
                case 4:
                    //Update ThingWorx variable file for technicians ease
                    AddStep("Updating Thingworx DefaultSessionVariablesSaveFile.txt");
                    FileHandler.UpdateThingworxXML(_Variables.PISN);
                    NextStep();
                    break;
                case 5:
                    //Restart PC for name change to take effect
                    AddStep("Restarting PC");
                    _MainProgram.RunAppOnStart(Task.GetTaskNameAtIndex(AllTasks.PI_SOFTWARE_INSTALLATION), (int)ResumeCodes.POST_RENAME_RESUME);
                    ProcessHandler.RestartPC();
                    break;
                case 6:
                    //Resuming from restart
                    _MainProgram.RunAppOnStart(false);
                    _Variables.PISN = Configuration_App_Instance.PiSn;
                    AddStep("Create PI Folder on shared drive");
                    AddStep("Running PC Rename Powershell Script");
                    AddStep("Store Rename task progress in INI");
                    AddStep("Setting App to restart on Windows Startup");
                    AddStep("Restarting PC");
                    NextStep();
                    break;
                default:
                    Configuration_App_Instance.PC_RENAME_STATE = TaskStates.PASS;
                    Complete();
                    break;
            }
        }

        internal override void Fail(Exception e)
        {
            Configuration_App_Instance.PC_RENAME_STATE = TaskStates.FAIL;
            base.Fail(e);
            
        }

        internal override string TaskName
        {
            get
            {
                return "Rename PC";
            }
        }
    }
}