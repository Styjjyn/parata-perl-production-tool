﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace _415_0603_Production_Tool_PI_PERL
{
    public class Configuration_App_Instance
    {

        private static Configuration_App_Instance link;

        private Settings settings;

        [XmlRoot]
        public class Settings
        {

            [XmlElement]
            public Application application = new Application();

            [XmlElement]
            public Instance instance = new Instance();

            [XmlElement]
            public Tasks tasks = new Tasks();

            public class Application
            {
                public string AppName = _CONSTANTS.APPLICATION_NAME;
                public string AppVersion = _CONSTANTS.APPLICATION_REV;
                public _MainProgram.RunModes RunMode = _MainProgram.RunModes.WORKSTATION;
            }

            public class Instance
            {
                public string PiSN = "";
                public string PiPN = "";
                public string WindowsKey = "";
                public string RestartTask = "";
                public int RestartCode = 0;

            }



            public class Tasks
            {

                public Task.TaskStates PcRenameState = Task.TaskStates.PENDING;
                public Task.TaskStates DonglePairState = Task.TaskStates.PENDING;
                public Task.TaskStates ThingworxState = Task.TaskStates.PENDING;
                public Task.TaskStates PlcUpdateState = Task.TaskStates.PENDING;
                public Task.TaskStates WinActState = Task.TaskStates.PENDING;
                public Task.TaskStates FinalFunctionalState = Task.TaskStates.PENDING;
                public Task.TaskStates TransportSpeedState = Task.TaskStates.PENDING;
                public Task.TaskStates DriveCheckState = Task.TaskStates.PENDING;
                internal Task.TaskStates InputPromptState = Task.TaskStates.PENDING;
            }

            public void Save()
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                using (FileStream stream = new FileStream(ConfigPath, FileMode.Create))
                {
                    serializer.Serialize(stream, this);
                    stream.Close();
                }
            }

            private static string ConfigPath
            { get { return _CONSTANTS.FOLDER_PATH_APP + _CONSTANTS.FILE_NAME_APP_CFG; } }

            public static Settings Load()
            {
                Settings ret;

                if (File.Exists(Settings.ConfigPath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                    using (FileStream stream = new FileStream(ConfigPath, FileMode.Open))
                    {
                        ret = serializer.Deserialize(stream) as Settings;
                        stream.Close();
                    }
                }
                else
                    ret = new Settings();

                return ret;
            }
        }

        public static string PiSn {
            get { return link.settings.instance.PiSN; }
            set { link.settings.instance.PiSN = value; link.settings.Save(); }
        }

        public static string PiPN {
            get { return link.settings.instance.PiPN; }
            set { link.settings.instance.PiPN = value; link.settings.Save(); }
        }

        public static string WindowsKey
        {
            get { return link.settings.instance.WindowsKey; }
            set { link.settings.instance.WindowsKey = value; link.settings.Save(); }
        }

        internal static string AppVersion { get { return link.settings.application.AppVersion; } }

        internal static int RestartCode {
            get { return link.settings.instance.RestartCode; }
            set { link.settings.instance.RestartCode = value; link.settings.Save(); }
        }

        public static Task.TaskStates PC_RENAME_STATE {
            get
            {
                return link.settings.tasks.PcRenameState;
            }
            set { link.settings.tasks.PcRenameState = value;link.settings.Save(); } }

        public static Task.TaskStates PLC_UPDATE_STATE {
            get
            {
                return link.settings.tasks.PlcUpdateState;
            }
            set { link.settings.tasks.PlcUpdateState = value; link.settings.Save(); } }

        public static Task.TaskStates DONGLE_PAIR_STATE {
            get
            {
                return link.settings.tasks.DonglePairState;
            }
            set { link.settings.tasks.DonglePairState = value; link.settings.Save(); } }

        public static Task.TaskStates THINGWORX_STATE {
            get
            {
                return link.settings.tasks.ThingworxState;
            }
            set { link.settings.tasks.ThingworxState = value; link.settings.Save(); } }

        public static Task.TaskStates WINDOWS_ACTIVATE_STATE {
            get
            {
                return link.settings.tasks.WinActState;
            }
            set { link.settings.tasks.WinActState = value; link.settings.Save(); } }

        public static _MainProgram.RunModes RunMode {
            get {
                return link.settings.application.RunMode;
            }
            set { link.settings.application.RunMode = value; link.settings.Save(); }
        }

        public static string RestartTask {
            get { return link.settings.instance.RestartTask; }
            set { link.settings.instance.RestartTask = value; link.settings.Save(); }
        }

        public static Task.TaskStates FIN_FUN_PREP_STATE {
            get { return link.settings.tasks.FinalFunctionalState; }
            set { link.settings.tasks.FinalFunctionalState = value; }
        }

        public static Task.TaskStates TRANSPORT_SPEED_CONFIG {
            get {
                return link.settings.tasks.TransportSpeedState;
            }
            set
            {
                link.settings.tasks.TransportSpeedState = value;
                link.settings.Save();
            } }

        public static Task.TaskStates DRIVE_CHECK_TASK {
            get {
                return link.settings.tasks.DriveCheckState;
            }
            set {
                link.settings.tasks.DriveCheckState = value;
                link.settings.Save();
            }
        }

        public static Task.TaskStates PROMPT_FOR_INPUT_STATE
        {
            get
            {
                return link.settings.tasks.InputPromptState;
            }
            set
            {
                link.settings.tasks.InputPromptState = value;
                link.settings.Save();
            }
        }
        internal void Init()
        {
            if (link == null)
                link = this;
            else
                return;
            //Check folder exists
            FileHandler.EnsureAppFolderExists();

            //Check File exists
            settings = Settings.Load();
            settings.Save();

        }

        
    }
}
