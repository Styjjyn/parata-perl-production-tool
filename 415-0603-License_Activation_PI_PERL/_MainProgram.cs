﻿using System;
using System.Windows.Forms;
using System.Security.Principal;
using System.Diagnostics;
using System.IO;

namespace _415_0603_Production_Tool_PI_PERL
{
    public static class _MainProgram
    {

        static MainForm mainForm;
#pragma warning disable CS0649
        private static bool qAMode = false;
#pragma warning restore CS0649
        
        public enum RunModes
        {
            PI,
            WORKSTATION
        }
        public static Configuration_App_Instance config;
        public static Configuration_App_Global emailConfig;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            
            try
            {
                //Convert old format Dongle INIs to new format XML
                //IniToXmlConverter.Do();

                config = new Configuration_App_Instance();
                emailConfig = new Configuration_App_Global();
                config.Init();

                if (IsOnPi() == true && IsAdministrator() == false)
                {

                    
                    ProcessStartInfo psInfo = new ProcessStartInfo();
                    psInfo.UseShellExecute = true;
                    psInfo.WorkingDirectory = Application.ExecutablePath;
                    psInfo.FileName = Application.ExecutablePath;
                    psInfo.Verb = "runas";
                    try
                    {
                        using (Process p = Process.Start(psInfo))
                        {
                            p.WaitForExit();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error starting: " + ex);
                        Application.Exit();
                    }

                }
                else
                {
                    try
                    {

                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);

                        Task.INIT();
                        FileHandler.EnsureAppFolderExists();
                        mainForm = new MainForm();
                        ThreadHandler.mainThreadClass = mainForm;
                        isRunning = true;


                        Application.Run(mainForm);

                        if (Event_OnProgramExit != null) Event_OnProgramExit.Invoke();

                        isRunning = false;
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Failed start: " + ex);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to startup:\b\r{ex}");
                throw;
            }
            

            
        }

        internal delegate void Delegate_OnProgramExit();
        internal static event Delegate_OnProgramExit Event_OnProgramExit;

        private static bool IsOnPi()
        {
            bool isOnPi = Directory.Exists(_CONSTANTS.FOLDER_PATH_PI_MAIN);
            if(isOnPi) Configuration_App_Instance.RunMode = RunModes.PI;
            else Configuration_App_Instance.RunMode = RunModes.WORKSTATION;

            return isOnPi;
        }

        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        internal static IntPtr Handle
        {
            get { return mainForm.Handle; }
        }

        internal static void RunAppOnStart(string taskToRunAtStart, int resumeCode)
        {
            RegistryHandler.RestartAppAtStartup(true);
            Configuration_App_Instance.RestartTask = taskToRunAtStart;
            Configuration_App_Instance.RestartCode = resumeCode;
        }

        internal static void RunAppOnStart(bool runOnStart)
        {
            RegistryHandler.RestartAppAtStartup(false);
            Configuration_App_Instance.RestartTask = "";
        }

        internal static FormWindowState WindowState { set { mainForm.WindowState = value; } }

        internal static bool QAMode
        {
            get { return qAMode; }
        }

        static bool isRunning;
        public delegate bool Delegate_Bool();
        public static bool IsRunning()
        {

            if (MainForm.link.InvokeRequired)
            {
                try
                {
                    return (bool)MainForm.link.Invoke(new Delegate_Bool(IsRunning));
                }
                catch (ObjectDisposedException)
                {
                    return false;
                }
                
            }
            else
                return isRunning;

        }
    }
}
