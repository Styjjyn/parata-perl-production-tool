﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _415_0603_Production_Tool_PI_PERL
{
    internal static class ErrorHandler
    {
        private static List<string> errors = new List<string>();

        internal enum Errors
        {
            NO_NETWORK_DRIVE_FOUND, //Could not find the shared drive path
            UNHANDLED_EMAIL_SEND_ERROR, //Have not determined source of this error yet
            OUTLOOK_NOT_INSTALLED, //Outlook could not be started on the workstation meaning either it isnt installed or something else went wrong.
            TOO_MANY_LICENSING_FOLDERS_FOUND,
            TOO_FEW_LICENSING_FOLDERS_FOUND,
            EMAIL_NOT_SENT,  //something prevented the email from being sent
            NO_PI_DONGLE_FOUND,
            FILE_EXISTS_NO_OVERWRITE,
            MIL_CONFIG_NOT_INSTALLED_ON_WORKSTATION,
            UNHANDLED_OPENING_MIL_CONFIG_ERROR,
            CLOSING_MIL_CONFIG_ERROR,
            EMAIL_HANDLER_INITIAILIZATION_ERROR,
            ERROR_MOVING_FOLDER_FROM_REQUEST_TO_PENDING,
            EMAIL_CANCELLED_BY_USER, //The user chose to not send the email
            THINGWORX_EASY_BUTTON_PROCESS_NOT_FOUND
        }

        private static void AddError(string s)
        {
            errors.Add(s);
        }

        internal static void AddError(Errors error)
        {
            AddError(error,"");
        }

        internal static void AddError(Errors error, string supplementalInfo)
        {
            if (supplementalInfo.Length > 0)
                supplementalInfo = ": \r\n\r\n" + supplementalInfo;

            AddError(error.ToString().Replace("_"," ").ToLower() + supplementalInfo);
        }

        internal static bool NoErrors
        {
            get { return errors.Count == 0; }
        }

        internal static void ResetErrors()
        {
            errors = new List<string>();
        }

        internal static string ListErrors()
        {
            string list = "";
            foreach (string s in errors)
            {
                list += "[]" + s + "\r\n";
            }

            
            return list;
        }
    }
}
