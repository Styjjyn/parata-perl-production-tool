﻿namespace _415_0603_Production_Tool_PI_PERL
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.comboBox_Tasks = new System.Windows.Forms.ToolStripComboBox();
            this.button_tool_GO = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuItem_Help_About = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskStateSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel_Steps = new System.Windows.Forms.TableLayoutPanel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label_Utility = new System.Windows.Forms.Label();
            this.label_Connection = new System.Windows.Forms.Label();
            this.bgWorker_sDrive = new System.ComponentModel.BackgroundWorker();
            this.bgworker_ProgressBar = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comboBox_Tasks,
            this.button_tool_GO,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(496, 33);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // comboBox_Tasks
            // 
            this.comboBox_Tasks.Name = "comboBox_Tasks";
            this.comboBox_Tasks.Size = new System.Drawing.Size(230, 33);
            this.comboBox_Tasks.Text = "Select Task";
            this.comboBox_Tasks.SelectedIndexChanged += new System.EventHandler(this.comboBox_Tasks_SelectedIndexChanged);
            this.comboBox_Tasks.Click += new System.EventHandler(this.comboBox_Tasks_Click);
            // 
            // button_tool_GO
            // 
            this.button_tool_GO.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.button_tool_GO.Image = ((System.Drawing.Image)(resources.GetObject("button_tool_GO.Image")));
            this.button_tool_GO.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_tool_GO.Name = "button_tool_GO";
            this.button_tool_GO.Size = new System.Drawing.Size(28, 30);
            this.button_tool_GO.Text = "button_tool_Go";
            this.button_tool_GO.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_Help_About,
            this.adminToolsToolStripMenuItem});
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(42, 30);
            this.toolStripButton1.Text = "About";
            // 
            // menuItem_Help_About
            // 
            this.menuItem_Help_About.Name = "menuItem_Help_About";
            this.menuItem_Help_About.Size = new System.Drawing.Size(210, 30);
            this.menuItem_Help_About.Text = "About";
            this.menuItem_Help_About.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // adminToolsToolStripMenuItem
            // 
            this.adminToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.taskStateSetToolStripMenuItem});
            this.adminToolsToolStripMenuItem.Name = "adminToolsToolStripMenuItem";
            this.adminToolsToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.adminToolsToolStripMenuItem.Text = "Admin Tools";
            // 
            // taskStateSetToolStripMenuItem
            // 
            this.taskStateSetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modifyToolStripMenuItem,
            this.resetAllToolStripMenuItem});
            this.taskStateSetToolStripMenuItem.Name = "taskStateSetToolStripMenuItem";
            this.taskStateSetToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.taskStateSetToolStripMenuItem.Text = "Task State";
            // 
            // modifyToolStripMenuItem
            // 
            this.modifyToolStripMenuItem.Name = "modifyToolStripMenuItem";
            this.modifyToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.modifyToolStripMenuItem.Text = "Modify";
            this.modifyToolStripMenuItem.Click += new System.EventHandler(this.modifyToolStripMenuItem_Click);
            // 
            // resetAllToolStripMenuItem
            // 
            this.resetAllToolStripMenuItem.Name = "resetAllToolStripMenuItem";
            this.resetAllToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.resetAllToolStripMenuItem.Text = "Reset All";
            this.resetAllToolStripMenuItem.Click += new System.EventHandler(this.resetAllToolStripMenuItem_Click);
            // 
            // panel_Steps
            // 
            this.panel_Steps.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.panel_Steps.AutoScroll = true;
            this.panel_Steps.ColumnCount = 1;
            this.panel_Steps.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.panel_Steps.Location = new System.Drawing.Point(14, 68);
            this.panel_Steps.Name = "panel_Steps";
            this.panel_Steps.RowCount = 2;
            this.panel_Steps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.panel_Steps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.panel_Steps.Size = new System.Drawing.Size(472, 214);
            this.panel_Steps.TabIndex = 1;
            this.panel_Steps.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Steps_Paint);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // label_Utility
            // 
            this.label_Utility.AutoSize = true;
            this.label_Utility.Location = new System.Drawing.Point(14, 289);
            this.label_Utility.Name = "label_Utility";
            this.label_Utility.Size = new System.Drawing.Size(0, 20);
            this.label_Utility.TabIndex = 2;
            // 
            // label_Connection
            // 
            this.label_Connection.AutoSize = true;
            this.label_Connection.Location = new System.Drawing.Point(14, 289);
            this.label_Connection.Name = "label_Connection";
            this.label_Connection.Size = new System.Drawing.Size(133, 20);
            this.label_Connection.TabIndex = 3;
            this.label_Connection.Text = "No S: Connection";
            this.label_Connection.Click += new System.EventHandler(this.label_Connection_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(266, 290);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(218, 23);
            this.progressBar1.TabIndex = 4;
            this.progressBar1.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 325);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label_Connection);
            this.Controls.Add(this.label_Utility);
            this.Controls.Add(this.panel_Steps);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "415-0603";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.Leave += new System.EventHandler(this.MainForm_Leave);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Help_About;
        private System.Windows.Forms.ToolStripComboBox comboBox_Tasks;
        private System.Windows.Forms.ToolStripButton button_tool_GO;
        private System.Windows.Forms.TableLayoutPanel panel_Steps;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label_Utility;
        private System.Windows.Forms.Label label_Connection;
        private System.ComponentModel.BackgroundWorker bgWorker_sDrive;
        private System.ComponentModel.BackgroundWorker bgworker_ProgressBar;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ToolStripMenuItem adminToolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taskStateSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetAllToolStripMenuItem;
    }
}

