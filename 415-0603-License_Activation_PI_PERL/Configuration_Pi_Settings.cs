﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using static System.Net.Mime.MediaTypeNames;

/// <summary>
/// This is the Export settings.xml that is generated from the Pi.Settings application
/// </summary>
namespace _415_0603_Production_Tool_PI_PERL
{
    public class Configuration_Pi_Settings
    {
        public static Configuration_Pi_Settings link;

        public ArrayOfConfig arrayOfconfig;
        public XDocument document;
        public string xmlPath;

        [XmlRoot("ArrayOfConfig")]
        public class ArrayOfConfig
        {
            [XmlElement("Config")]
            public List<Config> array;

            public void Save()
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ArrayOfConfig));

                using (FileStream stream = new FileStream($"C:\\Parata Systems\\Text.xml", FileMode.Create))
                {
                    serializer.Serialize(stream, this);
                    stream.Close();
                }
            }

            public static ArrayOfConfig Load(string s)
            {
                ArrayOfConfig ret = new ArrayOfConfig();
                
                XmlSerializer serializer = new XmlSerializer(typeof(ArrayOfConfig));

                using (FileStream stream = new FileStream($"{s}", FileMode.Open))
                {
                    ret = serializer.Deserialize(stream) as ArrayOfConfig;
                    stream.Close();
                }

                ret.Save();
                return ret;
            }
        }

        public void Init()
        {
            if (link == null)
                link = this;
        }

        public bool Check9010172Settings()
        {
            bool ret = true;
            MainForm.AddStep("Verifying 'SpeedNormal' = 90");
            ret &= arrayOfconfig.array.Where((config) => config.variable == "SpeedNormal").First().value == "90";
            if (ret == false) throw new Exception("SpeedNormal is incorrect");

            MainForm.AddStep("Verifying 'UserAmountLicense' = WyGWx5CU6kOFGU271a8asWs8+8Ex2Neu");
            ret &= arrayOfconfig.array.Where((config) => config.variable == "UserAmountLicense").First().value == "WyGWx5CU6kOFGU271a8asWs8+8Ex2Neu";
            if (ret == false) throw new Exception("UserAmount is incorrect");
            return ret;
        }
        
        
        public struct Config
        {
            public int id, createby;
            public DateTime createdate;
            public string objectName, tablename;
            public bool helper, GampLogChanges;
            public string type;
            public string value;
            public string variable;
            public int machinenumber;
            public bool iscurrentitem;

        }
    }
}
