﻿using _415_0603_Production_Tool_PI_PERL.Tasks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace _415_0603_Production_Tool_PI_PERL
{
    internal static class EMailHandler
    {
        static Outlook.MailItem mailItem;

        private static string emailRecipient;
        internal static string subject;
        internal static string htmlBody;
        private static string replyRecipients;

        private static class Vars
        {
            public static Outlook.Application app;
            public static Outlook.Explorer explorer;
        }

        private static Outlook.Application app
        {
            get {
                return Vars.app;
            }

            set {
                Vars.app = value;
                _MainProgram.Event_OnProgramExit += new _MainProgram.Delegate_OnProgramExit(()=>app.Quit());
            }
        }

        private static Outlook.Explorer explorer
        {
            get { return Vars.explorer; }
            set { Vars.explorer = value; }
        }
        
        
        internal static Outlook.MailItem SelectedEmail
        {
            get { return mailItem; }
            private set { mailItem = value; }
        }

        public delegate void Delegate_OnOutlookOpen();
        public static event Delegate_OnOutlookOpen Event_OnOutlookOpen;
        internal static void OpenOutlook()
        {
            try
            {
                app = Marshal.GetActiveObject("Outlook.Application") as Outlook.Application;
            }
            catch (Exception e)
            {
                app = new Outlook.Application();
            }
                
            
            mailItem = null;

            if (Event_OnOutlookOpen != null) Event_OnOutlookOpen.Invoke();
        }

        public delegate void Delegate_OnOutlookExplorerOpen();
        public static event Delegate_OnOutlookExplorerOpen Event_OnOutlookExplorerOpen;
        internal static void OpenOutlookWithExplorer()
        {
            if (app == null)
            {
                OpenOutlook();
                Outlook.Explorer explorer = app.Explorers.Add(
                        app.Session.GetDefaultFolder(
                        Outlook.OlDefaultFolders.olFolderInbox)
                        as Outlook.Folder,
                        Outlook.OlFolderDisplayMode.olFolderDisplayNormal);
                
                explorer.Display();
                explorer.WindowState = Outlook.OlWindowState.olMaximized;
                mailItem = null;
            }

            if (Event_OnOutlookExplorerOpen != null) Event_OnOutlookExplorerOpen.Invoke();
        }

        public delegate void Delegate_OnOutlookFilterOpen();
        public static event Delegate_OnOutlookFilterOpen Event_OnOutlookFilterOpen;
        internal static void OpenOutlook(string searchFilter)
        {
            OpenOutlook();
            ApplySearchFilter(searchFilter);

            if (Event_OnOutlookFilterOpen != null) Event_OnOutlookFilterOpen.Invoke();
        }
        public static void Event_OnOutlookFilterOpen_Clear()
        {
            Event_OnOutlookFilterOpen = null;
        }

        internal static void ApplySearchFilter(string filter)
        {
            if (app.Session.DefaultStore.IsInstantSearchEnabled)
            {
                explorer = app.Explorers.Add(
                    app.Session.GetDefaultFolder(
                    Outlook.OlDefaultFolders.olFolderInbox)
                    as Outlook.Folder,
                    Outlook.OlFolderDisplayMode.olFolderDisplayNormal);
                explorer.Search(filter,
                    Outlook.OlSearchScope.olSearchScopeAllFolders);
                
                explorer.Display();

                explorer.WindowState = Outlook.OlWindowState.olMaximized;

            }
        }

        internal static string carbonCopyRecipients;

        internal static void WaitForUserSelection()
        {
            explorer = app.ActiveExplorer();

            explorer.SelectionChange -= new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(Event_SelectionChange);

            explorer.SelectionChange += new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(Event_SelectionChange);
            SelectedEmail = null;

        }

        static Object selectedObj;
        static int selectionCount = 0;
        internal static void Event_SelectionChange()
        {
            try
            {
                if (app.ActiveExplorer().Selection.Count > 0)
                {
                    if (selectionCount == 0)
                    {
                        selectionCount++;
                        return;
                    }

                    selectedObj = app.ActiveExplorer().Selection[1];
                    
                    if (selectedObj is Outlook.MailItem)
                    {
                        (selectedObj as Outlook.MailItem).Display();
                        if (MainForm.ShowYesNoMessageBox("Correct EMail?", "Does this email contain the correct information?") == DialogResult.Yes)
                        {
                            SelectedEmail = selectedObj as Outlook.MailItem;
                            
                            explorer.SelectionChange -= new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(Event_SelectionChange);
                        }
                        else
                        {
                            selectionCount = 0;
                        }
                        
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        internal static bool manualSend = true;

        internal static string EmailRecipient { get { return emailRecipient; } set { emailRecipient = value; } }
        internal static string ReplyRecipients { get { return replyRecipients; } set { replyRecipients = value; } }

        internal static void CreateDVCEmail()
        {
            try
            {
                OpenOutlookWithExplorer();

                MainForm.link.WindowState = FormWindowState.Minimized;
                SelectedEmail = app.CreateItem(Outlook.OlItemType.olMailItem);

                EmailRecipient = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.DVC_TO);
                subject = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.DVC_SUBJECT);
                htmlBody = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.DVC_BODY);
                carbonCopyRecipients = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.DVC_CC);
                replyRecipients = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.DVC_REPLY_TO);

                SelectedEmail.Subject = subject.Replace("D###",_Variables.DongleSN);
                SelectedEmail.To = EmailRecipient;
                SelectedEmail.CC = carbonCopyRecipients;
                SelectedEmail.HTMLBody = htmlBody.Replace("D###",_Variables.DongleSN).Replace("L###",_Variables.LockCode);
                SelectedEmail.Importance = Outlook.OlImportance.olImportanceHigh;

                foreach (string s in (replyRecipients.Split(';')))
                {
                    SelectedEmail.ReplyRecipients.Add(s);
                }

                
                //listen for mail send event
                ((Outlook.ItemEvents_10_Event)SelectedEmail).Send += new Outlook.ItemEvents_10_SendEventHandler(MailSent);
                //listen for mail cancel event
                ((Outlook.ItemEvents_10_Event)SelectedEmail).Close += new Outlook.ItemEvents_10_CloseEventHandler(MailCanceled);

                SelectedEmail.Display(manualSend);
                     
                if (!manualSend)
                    SelectedEmail.Send();

            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                MainForm.FailLastStep(e);
            }
            catch (Exception e)
            {
                MainForm.FailLastStep(e);
            }
            if (!mailSent) throw new Exception("DVC Mail Send Operation Fail");

            MainForm.link.WindowState = FormWindowState.Normal;
        }

        internal static void CreateBPEmail()
        {
            try
            {
                OpenOutlookWithExplorer();

                MainForm.link.WindowState = FormWindowState.Minimized;
                SelectedEmail = app.CreateItem(Outlook.OlItemType.olMailItem);

                EmailRecipient = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.BP_TO);
                subject = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.BP_SUBJECT);
                htmlBody = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.BP_BODY);
                carbonCopyRecipients = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.BP_CC);
                replyRecipients = Configuration_App_Global.GetValue(Configuration_App_Global.SettingTarget.BP_REPLY_TO);


                SelectedEmail.Subject = subject.Replace("D###", _Variables.DongleSN);
                SelectedEmail.To = EmailRecipient;
                SelectedEmail.CC = carbonCopyRecipients;
                SelectedEmail.HTMLBody = htmlBody.Replace("D###", _Variables.DongleSN).Replace("LIC#", _Variables.LicType);
                SelectedEmail.Importance = Outlook.OlImportance.olImportanceHigh;

                foreach (string s in (replyRecipients.Split(';')))
                {
                    SelectedEmail.ReplyRecipients.Add(s);
                }

                //listen for mail send event
                ((Outlook.ItemEvents_10_Event)SelectedEmail).Send += new Outlook.ItemEvents_10_SendEventHandler(MailSent);
                //listen for mail cancel event
                ((Outlook.ItemEvents_10_Event)SelectedEmail).Close += new Outlook.ItemEvents_10_CloseEventHandler(MailCanceled);

                SelectedEmail.Display(manualSend);

                if (!manualSend)
                    SelectedEmail.Send();

            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }

            if (!mailSent) throw new Exception("BP Mail Send Operation Fail");

            MainForm.link.WindowState = FormWindowState.Normal;
        }

        static bool mailSent = false;
        private static void MailSent(ref bool sent)
        {
            mailSent = true;
            
        }

        private static void MailCanceled(ref bool cancel)
        {
            if (!mailSent)
                throw new Exception("Email send was cancelled by user");
        }

        internal static Dictionary<string,Outlook.Attachment> ScrubAttachmentsForMachLicNumber()
        {
            Dictionary<string, Outlook.Attachment> info = new Dictionary<string, Outlook.Attachment>();

            Outlook.Attachments attachments = SelectedEmail.Attachments;
            CultureInfo culture = CultureInfo.CurrentCulture;

            string fileName = "";

            foreach (Outlook.Attachment attachment in attachments)
            {
                fileName = attachment.FileName;

                //Search each attachments file name for 'machine.lic'
                if (culture.CompareInfo.IndexOf(fileName, "machine.lic", CompareOptions.IgnoreCase) >= 0)
                {
                    string[] split = fileName.Split('_');
                    if (split.Length >= 2)
                    {
                        info.Add(split[1], attachment);
                    }
                    else
                    {
                        throw new Exception("No machine.lic files found in selected email.");
                    }
                }
            }

            if (info.Count == 0)
            {
                throw new Exception("No machine.lic files found in selected email.");
            }

            return info;
        }
    }
}